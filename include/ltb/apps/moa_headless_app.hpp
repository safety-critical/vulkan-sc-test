// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/display/app_interface.hpp"
#include "ltb/vlk/vulkan_setup.hpp"

namespace ltb::apps
{

class MoaHeadlessApp
{
public:
    explicit MoaHeadlessApp( display::AppInterface& app, vlk::VulkanInterface& vulkan );

    auto initialize( vlk::VulkanSetupInterface& setup ) -> Result;

    auto run( ) -> void;

private:
    display::AppInterface& app_;
    vlk::VulkanSetupData   setup_;
};

} // namespace ltb::apps
