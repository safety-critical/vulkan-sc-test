// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/display/windowed_app_interface.hpp"
#include "ltb/vlk/vulkan_setup.hpp"

namespace ltb::apps
{

class MoaWindowedApp
{
public:
    explicit MoaWindowedApp(
        display::WindowedAppInterface& windowed_app,
        vlk::VulkanInterface&          vulkan
    );

    auto initialize( vlk::VulkanSetupInterface& setup ) -> Result;

    auto run( ) -> void;

private:
    vlk::VulkanSetupDataWithSurface setup_;
};

} // namespace ltb::apps
