// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/vlk/vlk.hpp"

// external
#include <spdlog/spdlog.h>

#define LTB_EXPAND( x ) x
#define LTB_GET2( _1, _2, NAME, ... ) NAME
#define LTB_GET3( _1, _2, _3, NAME, ... ) NAME

#define DETAIL_LTB_CHECK1( func )                                                                  \
    do                                                                                             \
    {                                                                                              \
        if ( auto const result = ( func ); result.type != ltb::ResultType::Success )               \
        {                                                                                          \
            return result;                                                                         \
        }                                                                                          \
    }                                                                                              \
    while ( false )

#define DETAIL_LTB_CHECK2( func, msg )                                                             \
    do                                                                                             \
    {                                                                                              \
        if ( auto const result = ( func ); result.type != ltb::ResultType::Success )               \
        {                                                                                          \
            spdlog::error(                                                                         \
                "{}: ({}, {})",                                                                    \
                msg,                                                                               \
                ltb::to_string( result.type ),                                                     \
                ltb::to_string( result.result )                                                    \
            );                                                                                     \
            return result;                                                                         \
        }                                                                                          \
    }                                                                                              \
    while ( false )

#define LTB_CHECK( ... )                                                                           \
    LTB_EXPAND( LTB_GET2( __VA_ARGS__, DETAIL_LTB_CHECK2, DETAIL_LTB_CHECK1, )( __VA_ARGS__ ) )

#define DETAIL_LTB_VK_CHECK1( func, result_type )                                                  \
    do                                                                                             \
    {                                                                                              \
        if ( auto const vk_result = ( func ); vk_result != VkResult::VK_SUCCESS )                  \
        {                                                                                          \
            return ltb::Result{ result_type, vk_result };                                          \
        }                                                                                          \
    }                                                                                              \
    while ( false )

#define DETAIL_LTB_VK_CHECK2( func, result_type, msg )                                             \
    do                                                                                             \
    {                                                                                              \
        if ( auto const vk_result = ( func ); vk_result != VkResult::VK_SUCCESS )                  \
        {                                                                                          \
            spdlog::error( "{} ({})", msg, ltb::to_string( vk_result ) );                          \
            return ltb::Result{ result_type, vk_result };                                          \
        }                                                                                          \
    }                                                                                              \
    while ( false )

#define LTB_VK_CHECK( ... )                                                                        \
    LTB_EXPAND( LTB_GET3( __VA_ARGS__, DETAIL_LTB_VK_CHECK2, DETAIL_LTB_VK_CHECK1, )( __VA_ARGS__  \
    ) )

namespace ltb
{

enum class ResultType
{
    Success,

    VulkanNotSupported,
    AppBackendInitFailed,
    WindowCreationFailed,

    VlkInstanceCreationFailed,
    VlkDebugMessengerNotSupported,
    VlkSurfaceCreationFailed,
    VlkPhysicalDeviceSelectionFailed,
};

class Result
{
public:
    ResultType type   = ResultType::Success;
    VkResult   result = VkResult::VK_SUCCESS;

    [[nodiscard]] static constexpr auto success( ) -> Result { return { }; }
};

auto operator==( Result const& lhs, Result const& rhs ) -> bool;
auto operator!=( Result const& lhs, Result const& rhs ) -> bool;

[[nodiscard]] auto to_string( ResultType result_type ) -> char const*;
[[nodiscard]] auto to_string( VkResult vk_result ) -> char const*;

} // namespace ltb
