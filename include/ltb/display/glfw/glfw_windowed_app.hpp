// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/display/windowed_app_interface.hpp"
#include "ltb/vlk/vlk.hpp"

// external
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

// standard
#include <memory>

namespace ltb::display
{

/// \brief A GLFW implementation of a windowed vulkan app.
class GlfwWindowedApp final : public WindowedAppInterface
{
public:
    ~GlfwWindowedApp( ) override = default;

    [[nodiscard]] auto initialize( ) -> Result override;

    [[nodiscard]]
    auto initialize_surface( VkInstance const& instance, VkSurfaceKHR* surface ) -> Result override;

    [[nodiscard]]
    auto should_exit( ) const -> bool override;
    auto poll_inputs( ) -> void override;

    auto on_framebuffer_resize( ResizeCallback callback, std::any user_data ) -> void override;

    [[nodiscard]]
    auto get_vulkan_instance_extensions( uint32* count ) const -> char const** override;

    struct Callbacks
    {
        ResizeCallback resize_callback  = nullptr;
        std::any       resize_user_data = nullptr;
    };

private:
    auto initialize_glfw( ) -> Result;
    auto initialize_window( ) -> Result;

    // This deleter pattern breaks if the class is copied :/
    int32                    glfw_         = 0;
    std::shared_ptr< int32 > glfw_deleter_ = nullptr;

    GLFWwindow*                   window_         = nullptr;
    std::shared_ptr< GLFWwindow > window_deleter_ = nullptr;

    Callbacks callbacks_ = { };
};

} // namespace ltb::display
