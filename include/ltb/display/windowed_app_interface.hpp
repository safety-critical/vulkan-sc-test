// A Logan Thomas Barnes project
#pragma once

// project
#include "ltb/display/app_interface.hpp"
#include "ltb/utils/types.hpp"

// external
#include <glm/glm.hpp>

// standard
#include <any>
#include <functional>

namespace ltb::display
{

/// \brief A wrapper around OS windowing functionality.
/// \details This interface allows classes to remain system
///          agnostic and enables mock system classes that
///          can be used to test error cases.
class WindowedAppInterface : public AppInterface
{
public:
    [[nodiscard]]
    virtual auto initialize_surface( VkInstance const& instance, VkSurfaceKHR* surface ) -> Result
                                                                                            = 0;

    /// \brief A callback type for when the window is resized.
    using ResizeCallback = std::function< void( glm::uvec2, std::any ) >;

    /// \brief Sets a callback for when the window is resized.
    /// \param callback the executed callback when the window is resized.
    /// \param user_data optional user data to pass to the callback.
    virtual auto on_framebuffer_resize( ResizeCallback callback, std::any user_data ) -> void = 0;

    /// \brief Returns a list of required VkInstance extensions for this surface.
    /// \param count the number of elements in the returned array.
    /// \return a list of required VkInstance extensions for this surface.
    [[nodiscard]]
    virtual auto get_vulkan_instance_extensions( uint32* count ) const -> char const** = 0;
};

} // namespace ltb::display
