// A Logan Thomas Barnes project
#pragma once

// project
#include "ltb/display/app_interface.hpp"

// standard
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

namespace ltb::display
{

/// \brief A headless terminal app that only exits when the user presses a key.
class TerminalApp : public AppInterface
{
public:
    explicit TerminalApp( );
    explicit TerminalApp( std::istream& input_stream );

    ~TerminalApp( ) override;

    [[nodiscard]] auto initialize( ) -> Result override;

    [[nodiscard]] auto should_exit( ) const -> bool override;

    auto poll_inputs( ) -> void override;

    auto block_until_polling_starts( ) -> void;
    auto block_until_polling_ends( ) -> void;

    struct InputHandler;

private:
    std::istream& input_stream_;
    bool          should_exit_ = false;

    bool                    begin_polling_     = false;
    bool                    currently_polling_ = false;
    std::mutex              poll_mutex_        = { };
    std::condition_variable poll_blocker_      = { };

    std::thread input_thread_ = { };
};

} // namespace ltb::display
