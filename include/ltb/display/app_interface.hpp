// A Logan Thomas Barnes project
#pragma once

// project
#include "ltb/utils/result.hpp"

namespace ltb::display
{

/// \brief A wrapper around basic application loop functionality.
/// \details This interface allows classes to remain system
///          agnostic and enables mock system classes that
///          can be used to test error cases.
class AppInterface
{
public:
    /// \brief Initializes the application.
    /// \return the result of the initialization.
    [[nodiscard]] virtual auto initialize( ) -> Result = 0;

    /// \brief Checks if the application should exit.
    [[nodiscard]] virtual auto should_exit( ) const -> bool = 0;

    /// \brief A non-blocking check for input events.
    virtual auto poll_inputs( ) -> void = 0;

protected:
    virtual ~AppInterface( ) = 0;
};

inline AppInterface::~AppInterface( ) = default;

} // namespace ltb::display
