// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/vlk/vulkan_setup_interface.hpp"

namespace ltb::vlk
{

class VulkanSetup : public VulkanSetupInterface
{
public:
    auto initialize( VulkanSetupData& setup ) const -> Result override;

    auto initialize( VulkanSetupDataWithSurface& setup ) const -> Result override;
};

} // namespace ltb::vlk
