// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/vlk/vulkan_interface.hpp"

namespace ltb::vlk
{

/// \brief A wrapper around the Vulkan graphics library.
class Vulkan : public VulkanInterface
{
public:
    auto get_instance_proc_addr( VkInstance_T* instance, char const* p_name ) const
        -> PFN_vkVoidFunction override;

    auto enumerate_physical_devices(
        VkInstance_T*     instance,
        uint32_t*         p_physical_device_count,
        VkPhysicalDevice* p_physical_devices
    ) const -> VkResult override;

    auto enumerate_device_extension_properties(
        VkPhysicalDevice_T*    physical_device,
        uint32_t*              p_extension_count,
        VkExtensionProperties* p_extensions
    ) const -> VkResult override;

    auto create_instance(
        VkInstanceCreateInfo const*  p_create_info,
        VkAllocationCallbacks const* p_allocator,
        VkInstance_T**               p_instance
    ) const -> VkResult override;

    auto destroy_instance( VkInstance_T* instance, VkAllocationCallbacks const* p_allocator ) const
        -> void override;

    auto destroy_surface(
        VkInstance_T*                instance,
        VkSurfaceKHR_T*              surface,
        VkAllocationCallbacks const* p_allocator
    ) const -> void override;

    auto get_physical_device_properties(
        VkPhysicalDevice_T*         physical_device,
        VkPhysicalDeviceProperties* p_properties
    ) const -> void override;

    auto get_physical_device_surface_support(
        VkPhysicalDevice_T* physical_device,
        uint32_t            queue_family_index,
        VkSurfaceKHR_T*     surface,
        VkBool32*           p_supported
    ) const -> VkResult override;

    auto get_physical_device_surface_formats(
        VkPhysicalDevice_T* physical_device,
        VkSurfaceKHR_T*     surface,
        uint32_t*           p_format_count,
        VkSurfaceFormatKHR* p_formats
    ) const -> VkResult override;

    auto get_physical_device_surface_present_modes(
        VkPhysicalDevice_T* physical_device,
        VkSurfaceKHR_T*     surface,
        uint32_t*           p_present_mode_count,
        VkPresentModeKHR*   p_present_modes
    ) const -> VkResult override;

    auto get_physical_device_features2(
        VkPhysicalDevice_T*        physical_device,
        VkPhysicalDeviceFeatures2* p_features
    ) const -> void override;

    auto get_physical_device_queue_family_properties(
        VkPhysicalDevice_T*      physical_device,
        uint32_t*                p_queue_family_count,
        VkQueueFamilyProperties* p_queue_families
    ) const -> void override;
};

} // namespace ltb::vlk
