// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/utils/result.hpp"
#include "ltb/utils/types.hpp"
#include "ltb/vlk/queue_types.hpp"
#include "ltb/vlk/vlk.hpp"

// standard
#include <array>
#include <limits>
#include <optional>
#include <vector>

namespace ltb::vlk
{

class PhysicalDeviceSelector
{
public:
    explicit PhysicalDeviceSelector( VulkanInterface& vulkan, Instance& instance );

    struct Initializer;

    [[nodiscard]]
    auto initialize( VkPhysicalDevice& selected_physical_device, QueueFamilyMap& queue_map ) const
        -> Result;

    [[nodiscard]]
    auto initialize(
        Initializer const& initializer,
        VkPhysicalDevice&  selected_physical_device,
        QueueFamilyMap&    queue_map
    ) const -> Result;

private:
    VulkanInterface& vulkan_;
    Instance&        instance_;

    auto initialize_physical_devices( std::vector< VkPhysicalDevice >& physical_devices
    ) const -> Result;
};

using PhysicalDeviceFeature = VkBool32 VkPhysicalDeviceFeatures::*;

struct PhysicalDeviceSelector::Initializer
{
    std::vector< char const* > extensions = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,
#if defined( __APPLE__ )
        "VK_KHR_portability_subset",
#endif
    };
    std::vector< PhysicalDeviceFeature > features = {
        &VkPhysicalDeviceFeatures::samplerAnisotropy,
    };
    std::vector< VkQueueFlagBits > queue_types = {
        VkQueueFlagBits::VK_QUEUE_GRAPHICS_BIT,
    };
    VkSurfaceKHR surface = nullptr;
};

struct SelectPhysicalDevice
{
    vlk::VulkanInterface& vulkan_;
    vlk::Instance&        instance_;
    VkPhysicalDevice&     physical_device_;
    QueueFamilyMap&       queue_families_;

    auto operator( )( VkSurfaceKHR_T* surface ) const -> Result;
};

} // namespace ltb::vlk
