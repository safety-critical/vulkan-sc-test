// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/display/windowed_app_interface.hpp"
#include "ltb/utils/result.hpp"
#include "ltb/vlk/vlk.hpp"
#include "ltb/vlk/vulkan_interface.hpp"

// standard
#include <memory>

namespace ltb::vlk
{

class Instance
{
public:
    explicit Instance( VulkanInterface& vulkan );

    struct Initializer;

    auto initialize( ) -> Result;
    auto initialize( Initializer const& initializer ) -> Result;

    auto initialize_surface( display::WindowedAppInterface& windowed_app, VkSurfaceKHR_T** surface )
        const -> Result;

    auto destroy_surface( VkSurfaceKHR_T* surface ) const -> void;

    auto enumerate_physical_devices( std::vector< VkPhysicalDevice >& physical_devices
    ) const -> Result;

    template < typename Callback >
    auto use( Callback const& callback ) const;

private:
    VulkanInterface& vulkan_;

    std::shared_ptr< VkInstance_T >               instance_        = nullptr;
    std::shared_ptr< VkDebugUtilsMessengerEXT_T > debug_messenger_ = nullptr;

    auto initialize_instance( Initializer const& initializer ) -> Result;
    auto initialize_debug_messenger( Initializer const& initializer ) -> Result;
};

template < typename Callback >
auto Instance::use( Callback const& callback ) const
{
    return callback( instance_.get( ) );
}

using DebugCallback = VKAPI_ATTR VkBool32 ( * )(
    VkDebugUtilsMessageSeverityFlagBitsEXT      message_severity,
    VkDebugUtilsMessageTypeFlagsEXT             message_type,
    VkDebugUtilsMessengerCallbackDataEXT const* callback_data,
    void*                                       user_data
);

VKAPI_ATTR VkBool32 default_debug_callback(
    VkDebugUtilsMessageSeverityFlagBitsEXT      message_severity,
    VkDebugUtilsMessageTypeFlagsEXT             message_type,
    VkDebugUtilsMessengerCallbackDataEXT const* callback_data,
    void*                                       user_data
);

struct Instance::Initializer
{
    std::string app_name = "Vulkan Application";

    std::vector< char const* > extension_names = {
#if defined( __APPLE__ )
        "VK_KHR_portability_enumeration",
#endif
#if !defined( NDEBUG )
        VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
#endif
    };

    std::vector< char const* > layer_names = {
#if !defined( LTB_DRIVEOS_DEVICE ) && !defined( NDEBUG ) && !defined( WIN32 )
        "VK_LAYER_KHRONOS_validation",
#endif
    };

    VkInstanceCreateFlags flags = {
#if defined( __APPLE__ )
        VK_INSTANCE_CREATE_ENUMERATE_PORTABILITY_BIT_KHR
#endif
    };

#if !defined( LTB_DRIVEOS_DEVICE ) && !defined( NDEBUG ) && !defined( WIN32 )
    DebugCallback debug_callback = default_debug_callback;
#else
    DebugCallback debug_callback = nullptr;
#endif
    bool manually_load_loader = false;
};

} // namespace ltb::vlk
