// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

#include "ltb/display/windowed_app_interface.hpp"
#include "ltb/utils/types.hpp"
#include "ltb/vlk/instance.hpp"
#include "ltb/vlk/queue_types.hpp"

// standard
#include <functional>
#include <memory>
#include <set>

namespace ltb::vlk
{

class Surface
{
public:
    explicit Surface( display::WindowedAppInterface& windowed_app, vlk::Instance& instance );

    auto initialize( ) -> Result;

    template < typename Callback, typename... Args >
    auto use( Callback&& callback, Args&&... args ) const;

private:
    display::WindowedAppInterface& windowed_app_;
    vlk::Instance&                 instance_;

    std::shared_ptr< VkSurfaceKHR_T > surface_ = nullptr;
};

template < typename Callback, typename... Args >
auto Surface::use( Callback&& callback, Args&&... args ) const
{
    return std::invoke(
        std::forward< Callback >( callback ),
        std::forward< Args >( args )...,
        surface_.get( )
    );
}

} // namespace ltb::vlk
