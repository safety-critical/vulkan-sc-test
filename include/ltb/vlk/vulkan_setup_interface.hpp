// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/display/windowed_app_interface.hpp"
#include "ltb/vlk/instance.hpp"
#include "ltb/vlk/surface.hpp"

namespace ltb::vlk
{

struct VulkanSetupData
{
    vlk::VulkanInterface& vulkan;

    vlk::Instance instance = vlk::Instance{ vulkan };

    VkPhysicalDevice            physical_device       = { };
    vlk::QueueFamilyMap         queue_families        = { };
    std::set< vlk::QueueIndex > unique_queue_families = { };
};

struct VulkanSetupDataWithSurface
{
    display::WindowedAppInterface& windowed_app;
    vlk::VulkanInterface&          vulkan;

    vlk::Instance instance = vlk::Instance{ vulkan };
    vlk::Surface  surface  = vlk::Surface{ windowed_app, instance };

    VkPhysicalDevice            physical_device       = { };
    vlk::QueueFamilyMap         queue_families        = { };
    std::set< vlk::QueueIndex > unique_queue_families = { };
};

/// \brief A class that initializes vulkan data structures
class VulkanSetupInterface
{
public:
    virtual auto initialize( VulkanSetupData& setup ) const -> Result = 0;

    virtual auto initialize( VulkanSetupDataWithSurface& setup ) const -> Result = 0;

protected:
    virtual ~VulkanSetupInterface( ) = 0;
};

inline VulkanSetupInterface::~VulkanSetupInterface( ) = default;

} // namespace ltb::vlk
