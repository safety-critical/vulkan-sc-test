// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/utils/types.hpp"

// standard
#include <functional>
#include <vector>

namespace ltb::vlk
{

/// \brief Enumerates a Vulkan object.
template < typename Value, typename Function, typename... Objects >
auto enumerate( std::vector< Value >& values, Function&& function, Objects&&... objects )
{
    auto count = uint32{ 0 };
    std::invoke(
        std::forward< Function >( function ),
        std::forward< Objects >( objects )...,
        &count,
        nullptr
    );
    values.resize( count );
    std::invoke(
        std::forward< Function >( function ),
        std::forward< Objects >( objects )...,
        &count,
        values.data( )
    );
}

template < typename Value, typename Function, typename... Objects >
auto enumerate_with_result(
    std::vector< Value >& values,
    Function&&            function,
    Objects&&... objects
) -> VkResult
{
    auto count  = uint32{ 0 };
    auto result = std::invoke(
        std::forward< Function >( function ),
        std::forward< Objects >( objects )...,
        &count,
        nullptr
    );
    if ( result == VkResult::VK_SUCCESS )
    {
        values.resize( count );
        result = std::invoke(
            std::forward< Function >( function ),
            std::forward< Objects >( objects )...,
            &count,
            values.data( )
        );
    }
    return result;
}

} // namespace ltb::vlk
