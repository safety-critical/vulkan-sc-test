// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/utils/result.hpp"
#include "ltb/utils/types.hpp"
#include "ltb/vlk/vlk.hpp"

// standard
#include <array>
#include <limits>
#include <optional>
#include <set>
#include <vector>

namespace ltb::vlk
{

constexpr auto detail_queue_type_start_line = __LINE__;
enum class QueueType : uint32
{
    Unknown       = 0U,
    Graphics      = 1U,
    Compute       = 2U,
    Transfer      = 3U,
    SparseBinding = 4U,
    Protected     = 5U,
    Surface       = 6U,
};
constexpr auto queue_type_count = __LINE__ - detail_queue_type_start_line - 4UL;

[[nodiscard]] auto to_string( QueueType type ) -> char const*;
[[nodiscard]] auto to_queue_type( VkQueueFlagBits flag ) -> QueueType;

[[nodiscard]]
auto to_queue_types( std::vector< VkQueueFlagBits > const& vk_types, VkSurfaceKHR const& surface )
    -> std::vector< QueueType >;

using QueueIndex = uint32;

class QueueFamilyMap
{
public:
    auto clear( ) -> void;

    auto add_supported_types(
        std::vector< VkQueueFlagBits > const&         types,
        std::vector< VkQueueFamilyProperties > const& queue_families,
        std::vector< QueueIndex > const&              surface_queue_family_indices,
        uint32                                        queue_family_index
    ) -> void;

    [[nodiscard]]
    auto all_types_supported( std::vector< QueueType > const& expected_types ) const -> bool;

    [[nodiscard]]
    auto get( QueueType type ) const -> std::optional< QueueIndex >;

    auto enumerate_unique_queue_families( std::set< vlk::QueueIndex >& unique_queue_families
    ) const -> void;

private:
    using IndexType = std::underlying_type_t< QueueType >;

    static constexpr auto invalid_queue_index_ = std::numeric_limits< QueueIndex >::max( );

    std::array< QueueIndex, queue_type_count > queue_families_ = {
        invalid_queue_index_,
        invalid_queue_index_,
        invalid_queue_index_,
        invalid_queue_index_,
        invalid_queue_index_,
        invalid_queue_index_,
        invalid_queue_index_,
    };

    class Checks;

    auto set( QueueType type, QueueIndex index ) -> void;
};

} // namespace ltb::vlk
