// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/ltb_config.hpp"

// external
#include <vulkan/vulkan.h>

namespace ltb::vlk
{

class Instance;
class PhysicalDeviceSelector;
class PhysicalDeviceInfo;
class QueueFamilyMap;
class Surface;
class VulkanInterface;

} // namespace ltb::vlk
