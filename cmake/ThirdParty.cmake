# ##############################################################
# A Logan Thomas Barnes project
# ##############################################################

### System Packages ###
if ("$ENV{DRIVEOS_VARIANT}" STREQUAL "linux")
  set(LTB_DRIVEOS_DEVICE TRUE)

  find_library(
    Vulkan_LIBRARY
    NAMES
    libvulkan.so.1
    HINTS
    /usr/lib/x86_64-linux-gnu
    REQUIRED
  )

  add_library(Vulkan::Vulkan INTERFACE IMPORTED)
  target_include_directories(
    Vulkan::Vulkan
    SYSTEM
    INTERFACE
    $ENV{NV_WORKSPACE}/drive-linux/include
  )
  target_link_libraries(
    Vulkan::Vulkan
    INTERFACE
    ${Vulkan_LIBRARY}
  )
  set(Vulkan_GLSLC_EXECUTABLE $ENV{NV_WORKSPACE}/drive-linux/tools/openGL-tools/glslc)
else ()
  find_package(Vulkan REQUIRED)
endif ()

### External Repositories ###
cpmaddpackage("gh:gabime/spdlog@1.13.0")
cpmaddpackage(
  NAME
  glm
  # None of the tagged versions have the fixes we need
  GIT_TAG
  1.0.0
  GITHUB_REPOSITORY
  g-truc/glm
  # It's header only and the CMakeLists.txt file adds
  # global flags that break CUDA on windows
  DOWNLOAD_ONLY
  TRUE
)
if (LTB_VST_USE_GLFW)
  cpmaddpackage(
    NAME
    GLFW
    GITHUB_REPOSITORY
    glfw/glfw
    GIT_TAG
    3.3.9
    OPTIONS
    "GLFW_BUILD_TESTS OFF"
    "GLFW_BUILD_EXAMPLES OFF"
    "GLFW_BUILD_DOCS OFF"
  )
endif ()

if (spdlog_ADDED)
  # Mark external include directories as system includes to avoid warnings.
  target_include_directories(
    spdlog
    SYSTEM
    INTERFACE
    $<BUILD_INTERFACE:${spdlog_SOURCE_DIR}/include>
  )
endif ()

if (glm_ADDED)
  add_library(
    glm
    INTERFACE
  )
  add_library(
    glm::glm
    ALIAS
    glm
  )
  target_compile_definitions(
    glm
    INTERFACE
    GLM_FORCE_RADIANS
    GLM_ENABLE_EXPERIMENTAL
    GLM_FORCE_EXPLICIT_CTOR
  )
  target_include_directories(
    glm
    SYSTEM
    INTERFACE
    # Mark external include directories as system includes to avoid warnings.
    $<BUILD_INTERFACE:${glm_SOURCE_DIR}>
  )
  target_compile_features(
    glm
    INTERFACE
    cxx_std_17
  )
endif ()

if (GLFW_ADDED)
  add_library(
    glfw::glfw
    ALIAS
    glfw
  )
endif ()
