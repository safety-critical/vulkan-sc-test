# Vulkan SC Test

[Code](https://gitlab.com/safety-critical/vulkan-sc-test)
| [Analysis](https://sonarcloud.io/project/overview?id=safety-critical_vulkan-sc-test)

## Resources

https://en.wikipedia.org/wiki/DO-178C

https://thecloudstrap.com/tools-for-achieving-do-178c-compliance/#Static_Analysis_Tools

https://www.sonarsource.com/products/sonarcloud/
