// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/display/windowed_app_interface.hpp"

// external
#include <gmock/gmock.h>

namespace ltb::test
{

/// \brief A mock interface for the WindowedAppInterface.
/// \details Allows tests to specify which functions should
///          be called and what they should return.
class MockWindowedApp : public display::WindowedAppInterface
{
public:
    ~MockWindowedApp( ) override = default;

    MOCK_METHOD( Result, initialize, ( ), ( override ) );

    MOCK_METHOD( bool, should_exit, ( ), ( const, override ) );

    MOCK_METHOD( void, poll_inputs, ( ), ( override ) );

    MOCK_METHOD( Result, initialize_surface, ( VkInstance const&, VkSurfaceKHR* ), ( override ) );

    void on_framebuffer_resize( ResizeCallback, std::any const ) override
    {
        // std::any doesn't work well with gmock so we just define this function normally.
    }

    MOCK_METHOD( char const**, get_vulkan_instance_extensions, ( uint32* ), ( const, override ) );
};

} // namespace ltb::test
