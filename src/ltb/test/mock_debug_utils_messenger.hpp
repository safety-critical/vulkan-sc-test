// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/vlk/vlk.hpp"

// external
#include <gmock/gmock.h>

namespace ltb::test
{

class DebugUtilsMessengerInterface
{
public:
    virtual auto create_debug_utils_messenger(
        VkInstance_T*                             instance,
        VkDebugUtilsMessengerCreateInfoEXT const* p_create_info,
        VkAllocationCallbacks const*              p_allocator,
        VkDebugUtilsMessengerEXT_T*               p_debug_messenger
    ) const -> VkResult = 0;

    virtual auto destroy_debug_utils_messenger(
        VkInstance_T*                instance,
        VkDebugUtilsMessengerEXT_T*  p_debug_messenger,
        VkAllocationCallbacks const* p_allocator
    ) const -> void = 0;

protected:
    virtual ~DebugUtilsMessengerInterface( ) = default;
};

class MockDebugUtilsMessenger : public DebugUtilsMessengerInterface
{
public:
    ~MockDebugUtilsMessenger( ) override = default;

    MOCK_METHOD(
        VkResult,
        create_debug_utils_messenger,
        ( VkInstance_T*,
          VkDebugUtilsMessengerCreateInfoEXT const*,
          VkAllocationCallbacks const*,
          VkDebugUtilsMessengerEXT_T* ),
        ( const, override )
    );

    MOCK_METHOD(
        void,
        destroy_debug_utils_messenger,
        ( VkInstance_T*, VkDebugUtilsMessengerEXT_T*, VkAllocationCallbacks const* ),
        ( const, override )
    );
};

} // namespace ltb::test
