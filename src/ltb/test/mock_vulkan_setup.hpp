// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/vlk/vulkan_setup_interface.hpp"

// external
#include <gmock/gmock.h>

namespace ltb::test
{

class MockVulkanSetup : public vlk::VulkanSetupInterface
{
public:
    MOCK_METHOD( Result, initialize, ( vlk::VulkanSetupData & setup ), ( const, override ) );

    MOCK_METHOD(
        Result,
        initialize,
        ( vlk::VulkanSetupDataWithSurface & setup ),
        ( const, override )
    );
};

} // namespace ltb::test
