// A Logan Thomas Barnes project
#pragma once

// project
#include "ltb/test/logging_capture.hpp"

// external
#include <gtest/gtest.h>

namespace ltb::test
{

auto get_test_name( ) -> std::string;

class LogCaptureTest : public ::testing::Test
{
public:
    LogCaptureTest( );

    LogCapture log_capture = LogCapture( get_test_name( ), spdlog::level::trace );
};

} // namespace ltb::test
