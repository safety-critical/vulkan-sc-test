// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/utils/ignore.hpp"
#include "ltb/vlk/vulkan_interface.hpp"

// external
#include <gmock/gmock.h>

namespace ltb::test
{

/// \brief A mock interface for the VulkanInterface.
/// \details Allows tests to specify which functions should
///          be called and what they should return.
class MockVulkan : public vlk::VulkanInterface
{
public:
    ~MockVulkan( ) override = default;

    MOCK_METHOD(
        PFN_vkVoidFunction,
        get_instance_proc_addr,
        ( VkInstance_T*, char const* ),
        ( const, override )
    );

    MOCK_METHOD(
        VkResult,
        enumerate_physical_devices,
        ( VkInstance_T*, uint32_t*, VkPhysicalDevice* ),
        ( const, override )
    );

    MOCK_METHOD(
        VkResult,
        enumerate_device_extension_properties,
        ( VkPhysicalDevice_T*, uint32_t*, VkExtensionProperties* ),
        ( const, override )
    );

    MOCK_METHOD(
        VkResult,
        create_instance,
        ( VkInstanceCreateInfo const*, VkAllocationCallbacks const*, VkInstance_T** ),
        ( const, override )
    );

    MOCK_METHOD(
        void,
        destroy_instance,
        ( VkInstance_T*, VkAllocationCallbacks const* ),
        ( const, override )
    );

    MOCK_METHOD(
        void,
        destroy_surface,
        ( VkInstance_T*, VkSurfaceKHR_T*, VkAllocationCallbacks const* ),
        ( const, override )
    );

    MOCK_METHOD(
        void,
        get_physical_device_properties,
        ( VkPhysicalDevice_T*, VkPhysicalDeviceProperties* ),
        ( const, override )
    );

    MOCK_METHOD(
        VkResult,
        get_physical_device_surface_support,
        ( VkPhysicalDevice_T*, uint32_t, VkSurfaceKHR_T*, VkBool32* ),
        ( const, override )
    );

    MOCK_METHOD(
        VkResult,
        get_physical_device_surface_formats,
        ( VkPhysicalDevice_T*, VkSurfaceKHR_T*, uint32_t*, VkSurfaceFormatKHR* ),
        ( const, override )
    );

    MOCK_METHOD(
        VkResult,
        get_physical_device_surface_present_modes,
        ( VkPhysicalDevice_T*, VkSurfaceKHR_T*, uint32_t*, VkPresentModeKHR* ),
        ( const, override )
    );

    MOCK_METHOD(
        void,
        get_physical_device_features2,
        ( VkPhysicalDevice_T*, VkPhysicalDeviceFeatures2* ),
        ( const, override )
    );

    MOCK_METHOD(
        void,
        get_physical_device_queue_family_properties,
        ( VkPhysicalDevice_T*, uint32_t*, VkQueueFamilyProperties* ),
        ( const, override )
    );
};

} // namespace ltb::test
