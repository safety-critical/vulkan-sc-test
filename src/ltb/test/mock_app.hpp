// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/display/app_interface.hpp"

// external
#include <gmock/gmock.h>

namespace ltb::test
{

/// \brief A mock interface for the AppInterface.
/// \details Allows tests to specify which functions should
///          be called and what they should return.
class MockApp : public display::AppInterface
{
public:
    MOCK_METHOD( Result, initialize, ( ), ( override ) );

    MOCK_METHOD( bool, should_exit, ( ), ( const, override ) );

    MOCK_METHOD( void, poll_inputs, ( ), ( override ) );
};

} // namespace ltb::test
