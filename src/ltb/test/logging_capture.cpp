// A Logan Thomas Barnes project
#include "ltb/test/logging_capture.hpp"

// external
#include <spdlog/sinks/ostream_sink.h>
#include <spdlog/spdlog.h>

// standard
#include <regex>

namespace ltb::test
{

LogCapture::LogCapture( std::string const& name, spdlog::level::level_enum const level )
    // https://github.com/gabime/spdlog/wiki/3.-Custom-formatting
    : LogCapture( name, level, "[%l] %v" )
{
}

LogCapture::LogCapture(
    std::string const&              name,
    spdlog::level::level_enum const level,
    std::string const&              pattern
)
{
    previous_logger_ = spdlog::default_logger( );

    // Capture spdlog output in `log_stream_`.
    auto ostream_logger = spdlog::get( name );
    if ( !ostream_logger )
    {
        auto ostream_sink = std::make_shared< spdlog::sinks::ostream_sink_st >( log_stream_ );
        ostream_logger    = std::make_shared< spdlog::logger >( name, ostream_sink );
        ostream_logger->set_pattern( pattern );
        ostream_logger->set_level( level );
    }
    spdlog::set_default_logger( std::move( ostream_logger ) );
}

LogCapture::~LogCapture( )
{
    spdlog::set_default_logger( std::move( previous_logger_ ) );
}

auto LogCapture::get_and_clear_logs( ) -> std::string
{
    auto const logs = log_stream_.str( );
    log_stream_.str( "" );
    return std::regex_replace( logs, std::regex( "\r\n" ), "\n" );
}

} // namespace ltb::test
