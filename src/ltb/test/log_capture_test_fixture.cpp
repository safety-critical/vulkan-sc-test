// A Logan Thomas Barnes project
#include "ltb/test/log_capture_test_fixture.hpp"

namespace ltb::test
{

auto get_test_name( ) -> std::string
{
    auto const* const test_info = ::testing::UnitTest::GetInstance( )->current_test_info( );
    return fmt::format( "{}_{}", test_info->test_case_name( ), test_info->name( ) );
}

LogCaptureTest::LogCaptureTest( )
{
    spdlog::set_level( spdlog::level::trace );
}

} // namespace ltb::test
