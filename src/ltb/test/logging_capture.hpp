// A Logan Thomas Barnes project
#pragma once

// external
#include <spdlog/spdlog.h>

// standard
#include <sstream>

namespace ltb::test
{

/// \brief A class that allows spdlog logging to be captured
///        in a string stream instead of displayed to std out.
class LogCapture
{
public:
    /// \brief Setup the capture stream.
    /// \param name - the name of the logger in the spdlog registry.
    /// \param level - the log level of the capture stream.
    LogCapture( std::string const& name, spdlog::level::level_enum level );

    /// \brief Setup the capture stream.
    /// \param name - the name of the logger in the spdlog registry.
    /// \param level - the log level of the capture stream.
    /// \param pattern - the output pattern that is written to the stream.
    LogCapture(
        std::string const&        name,
        spdlog::level::level_enum level,
        std::string const&        pattern
    );

    LogCapture( LogCapture const& )     = delete;
    LogCapture( LogCapture&& ) noexcept = delete;

    ~LogCapture( );

    auto operator=( LogCapture const& ) -> LogCapture&     = delete;
    auto operator=( LogCapture&& ) noexcept -> LogCapture& = delete;

    /// \brief Extracts the latest log messages and clears the stream.
    auto get_and_clear_logs( ) -> std::string;

private:
    std::ostringstream                log_stream_      = { };
    std::shared_ptr< spdlog::logger > previous_logger_ = { };
};

} // namespace ltb::test
