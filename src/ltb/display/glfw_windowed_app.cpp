// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#include "ltb/display/glfw/glfw_windowed_app.hpp"

// project
#include "ltb/utils/ignore.hpp"

// external
#include <spdlog/spdlog.h>

namespace ltb::display
{
namespace
{

auto default_error_callback( int32 const error, char const* description ) -> void
{
    spdlog::error( "GLFW Error ({}): {}", error, description );
}

auto terminate_glfw( int32 const* const )
{
    ::glfwTerminate( );
    spdlog::debug( "glfwTerminate()" );
}

auto destroy_window( GLFWwindow* const window )
{
    ::glfwDestroyWindow( window );
    spdlog::debug( "glfwDestroyWindow()" );
}

auto resize_callback( GLFWwindow* const window, int32 const width, int32 const height ) -> void
{
    auto const* const callbacks
        = static_cast< GlfwWindowedApp::Callbacks const* >( ::glfwGetWindowUserPointer( window ) );

    callbacks->resize_callback(
        glm::uvec2( static_cast< uint32 >( width ), static_cast< uint32 >( height ) ),
        callbacks->resize_user_data
    );
}

} // namespace

auto GlfwWindowedApp::initialize( ) -> Result
{
    LTB_CHECK( initialize_glfw( ) );
    LTB_CHECK( initialize_window( ) );

    return Result::success( );
}

auto GlfwWindowedApp::initialize_surface( VkInstance const& instance, VkSurfaceKHR* const surface )
    -> Result
{
    LTB_VK_CHECK(
        ::glfwCreateWindowSurface( instance, window_, nullptr, surface ),
        ResultType::VlkSurfaceCreationFailed
    );
    spdlog::debug( "glfwCreateWindowSurface()" );

    return Result::success( );
}

auto GlfwWindowedApp::should_exit( ) const -> bool
{
    return ::glfwWindowShouldClose( window_ );
}

auto GlfwWindowedApp::poll_inputs( ) -> void
{
    ::glfwPollEvents( );
}

auto GlfwWindowedApp::on_framebuffer_resize( ResizeCallback callback, std::any user_data ) -> void
{
    callbacks_.resize_callback  = std::move( callback );
    callbacks_.resize_user_data = std::move( user_data );

    // Ignore the returned previous callback since it is not used.
    utils::ignore( ::glfwSetFramebufferSizeCallback( window_, resize_callback ) );
}

auto GlfwWindowedApp::get_vulkan_instance_extensions( uint32* const count ) const -> char const**
{
    return ::glfwGetRequiredInstanceExtensions( count );
}

auto GlfwWindowedApp::initialize_glfw( ) -> Result
{
    // Set error callback before any other GLFW calls so errors are reported.
    // We can ignore the previous callback (the returned value).
    utils::ignore( ::glfwSetErrorCallback( default_error_callback ) );

    if ( glfw_ = ::glfwInit( ); GLFW_FALSE == glfw_ )
    {
        return { ResultType::AppBackendInitFailed };
    }
    spdlog::debug( "glfwInit()" );

    glfw_deleter_ = std::shared_ptr< int32 >( &glfw_, terminate_glfw );

    if ( GLFW_FALSE == ::glfwVulkanSupported( ) )
    {
        return { ResultType::VulkanNotSupported };
    }

    return Result::success( );
}

auto GlfwWindowedApp::initialize_window( ) -> Result
{
    ::glfwWindowHint( GLFW_CLIENT_API, GLFW_NO_API );
    ::glfwWindowHint( GLFW_RESIZABLE, GLFW_FALSE );

    constexpr auto width  = 1280;
    constexpr auto height = 720;

    window_ = ::glfwCreateWindow( width, height, "Vulkan Application", nullptr, nullptr );

    if ( nullptr == window_ )
    {
        return { ResultType::WindowCreationFailed };
    }
    spdlog::debug( "glfwCreateWindow()" );

    window_deleter_ = std::shared_ptr< GLFWwindow >( window_, destroy_window );

    ::glfwSetWindowUserPointer( window_, &callbacks_ );

    return Result::success( );
}

} // namespace ltb::display
