// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#include "ltb/display/terminal_app.hpp"

// project
#include "ltb/utils/ignore.hpp"

// external
#include <spdlog/spdlog.h>

// standard
#include <iostream>

namespace ltb::display
{

struct TerminalApp::InputHandler
{
    TerminalApp& app;

    auto operator( )( ) const -> void
    {
        // Block until the app is ready to poll for input. This blocks
        // in a way we can control unlike the std::cin.get() call below.
        block_until_ready( );

        if ( !app.should_exit_ )
        {
            spdlog::info( "Press Enter to exit." );
            app.currently_polling_ = true;
            app.poll_blocker_.notify_one( );

            // Block until the user presses enter
            utils::ignore( app.input_stream_.get( ) );

            spdlog::info( "Exiting..." );
            app.should_exit_ = true;

            app.currently_polling_ = false;
            app.poll_blocker_.notify_one( );
        }
    }

    auto block_until_ready( ) const -> void
    {
        auto lock = std::unique_lock( app.poll_mutex_ );
        app.poll_blocker_.wait( lock, [ this ] { return app.begin_polling_; } ); // NOSONAR
    }
};

TerminalApp::TerminalApp( )
    : TerminalApp( std::cin )
{
}

TerminalApp::TerminalApp( std::istream& input_stream )
    : input_stream_{ input_stream }
{
}

TerminalApp::~TerminalApp( )
{
    if ( input_thread_.joinable( ) )
    {
        should_exit_   = true;
        begin_polling_ = true;
        poll_blocker_.notify_one( );
        input_thread_.join( );
    }
}

auto TerminalApp::initialize( ) -> Result
{
    // Initialize the input thread but don't block on user input yet.
    input_thread_ = std::thread{ InputHandler{ *this } };
    return Result::success( );
}

auto TerminalApp::should_exit( ) const -> bool
{
    return should_exit_;
}

auto TerminalApp::poll_inputs( ) -> void
{
    // Initialization passed. The input thread can now block until the user presses enter.
    begin_polling_ = true;
    poll_blocker_.notify_one( );
}

auto TerminalApp::block_until_polling_starts( ) -> void
{
    auto lock = std::unique_lock( poll_mutex_ );
    poll_blocker_.wait( lock, [ this ] { return currently_polling_; } ); // NOSONAR
}

auto TerminalApp::block_until_polling_ends( ) -> void
{
    auto lock = std::unique_lock( poll_mutex_ );
    poll_blocker_.wait( lock, [ this ] { return !currently_polling_; } ); // NOSONAR
}

} // namespace ltb::display
