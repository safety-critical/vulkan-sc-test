// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////

// project
#include "ltb/display/terminal_app.hpp"
#include "ltb/test/log_capture_test_fixture.hpp"

namespace
{

class BlockingBuffer : public std::streambuf
{
public:
    auto underflow( ) -> int_type override
    {
        auto lock = std::unique_lock{ mutex_ };
        blocker_.wait( lock, [ this ] { return !keep_blocking_; } ); // NOSONAR
        return traits_type::eof( );
    }

    auto overflow( int_type const ) -> int_type override { return traits_type::eof( ); }

    auto unblock( ) -> void
    {
        auto const lock = std::scoped_lock{ mutex_ };
        keep_blocking_  = false;
        blocker_.notify_one( );
    }

private:
    bool                    keep_blocking_ = true;
    std::mutex              mutex_         = { };
    std::condition_variable blocker_       = { };
};

class TerminalAppTests : public ltb::test::LogCaptureTest
{
};

TEST_F( TerminalAppTests, ExitsWithoutPollInputs )
{
    auto app = ltb::display::TerminalApp{ };

    EXPECT_EQ( ltb::Result{ }, app.initialize( ) );

    EXPECT_FALSE( app.should_exit( ) );
}

TEST_F( TerminalAppTests, UnblocksAfterTextInputAndExits )
{
    using namespace std::chrono_literals;

    auto blocking_cin = BlockingBuffer{ };
    auto mock_cin     = std::istream{ &blocking_cin };

    auto app = std::make_shared< ltb::display::TerminalApp >( mock_cin );

    EXPECT_EQ( ltb::Result{ }, app->initialize( ) );

    EXPECT_FALSE( app->should_exit( ) );
    app->poll_inputs( );

    app->block_until_polling_starts( );
    EXPECT_EQ( log_capture.get_and_clear_logs( ), "[info] Press Enter to exit.\n" );

    EXPECT_FALSE( app->should_exit( ) );
    blocking_cin.unblock( );

    // Wait for the input thread to exit upon deletion.
    app = nullptr;

    EXPECT_EQ( log_capture.get_and_clear_logs( ), "[info] Exiting...\n" );
}

TEST_F( TerminalAppTests, UnblocksAfterTextInputShouldExit )
{
    using namespace std::chrono_literals;

    auto blocking_cin = BlockingBuffer{ };
    auto mock_cin     = std::istream{ &blocking_cin };

    auto app = ltb::display::TerminalApp{ mock_cin };

    EXPECT_EQ( ltb::Result{ }, app.initialize( ) );

    EXPECT_FALSE( app.should_exit( ) );
    app.poll_inputs( );

    app.block_until_polling_starts( );
    EXPECT_EQ( log_capture.get_and_clear_logs( ), "[info] Press Enter to exit.\n" );

    EXPECT_FALSE( app.should_exit( ) );
    blocking_cin.unblock( );

    app.block_until_polling_ends( );
    EXPECT_TRUE( app.should_exit( ) );

    EXPECT_EQ( log_capture.get_and_clear_logs( ), "[info] Exiting...\n" );
}

} // namespace
