// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////

// project
#include "ltb/apps/moa_windowed_app.hpp"
#include "ltb/test/mock_vulkan.hpp"
#include "ltb/test/mock_vulkan_setup.hpp"
#include "ltb/test/mock_windowed_app.hpp"

// external
#include <gmock/gmock.h>

struct VkSurfaceKHR_T
{
    // Fake Surface Type
};

struct VkPhysicalDevice_T
{
    // Fake Physical Device Type
    explicit VkPhysicalDevice_T( ltb::int32 fake_id )
        : id( fake_id )
    {
    }

    ltb::int32 id;
};

namespace ltb
{
namespace
{

class Extensions
{
public:
    auto operator( )( uint32* const count ) -> char const**
    {
        *count = static_cast< uint32 >( extensions_.size( ) );
        return extensions_.data( );
    };

private:
    std::array< char const*, 1 > extensions_ = { "VK_KHR_surface" };
};

struct SetSurface
{
    VkSurfaceKHR_T& surface_;
    Result const&   returned_result;

    auto operator( )( VkInstance const&, VkSurfaceKHR* surface ) const -> Result
    {
        *surface = &surface_;
        return returned_result;
    }
};

struct SetPhysicalDevices
{
    std::vector< VkPhysicalDevice >& physical_devices_;
    VkResult const&                  returned_result;

    auto operator( )( VkInstance_T*, uint32_t* count, VkPhysicalDevice* devices ) const -> VkResult
    {
        *count = static_cast< uint32_t >( physical_devices_.size( ) );
        if ( devices )
        {
            std::copy( physical_devices_.begin( ), physical_devices_.end( ), devices );
        }
        return returned_result;
    }
};

class MoaWindowedAppTests : public ::testing::Test
{
public:
    testing::StrictMock< test::MockWindowedApp > mock_app          = { };
    testing::StrictMock< test::MockVulkan >      mock_vulkan       = { };
    testing::StrictMock< test::MockVulkanSetup > mock_vulkan_setup = { };
};

#if 0
TEST_F( MoaWindowedAppTests, InitializeSuccess )
{
    auto app = apps::MoaWindowedApp{ *mock_app, *mock_vulkan };

    auto const result = Result::success( );

    // Make sure the calls happen in this exact order.
    auto const s = ::testing::Sequence{ };

    // Window initialization
    EXPECT_CALL( *mock_app, initialize ).InSequence( s ).WillOnce( testing::Return( result ) );

    // Instance initialization
    EXPECT_CALL( *mock_app, get_vulkan_instance_extensions )
        .InSequence( s )
        .WillOnce( Extensions{ } );
    EXPECT_CALL( *mock_vulkan, create_instance )
        .InSequence( s )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) );
    EXPECT_CALL( *mock_vulkan, get_instance_proc_addr )
        .InSequence( s )
        .WillOnce( testing::Return( create_debug_utils_messenger_proc_addr ) );
    EXPECT_CALL( *mock_vulkan, get_instance_proc_addr )
        .InSequence( s )
        .WillOnce( testing::Return( destroy_debug_utils_messenger_proc_addr ) );

    VkSurfaceKHR_T fake_surface;

    // Surface initialization
    EXPECT_CALL( *mock_app, initialize_surface )
        .InSequence( s )
        .WillOnce( SetSurface{ fake_surface, result } );

    // Physical device selection
#if 0
    auto fake_physical_device1 = std::make_shared< VkPhysicalDevice_T >( 1 );
    auto fake_physical_device2 = std::make_shared< VkPhysicalDevice_T >( 1 );

    auto fake_physical_devices = std::vector{
        fake_physical_device1.get( ),
        fake_physical_device2.get( ),
    };

    EXPECT_CALL( *mock_vulkan, enumerate_physical_devices )
        .InSequence( s )
        .WillOnce( SetPhysicalDevices{ fake_physical_devices, VkResult::VK_SUCCESS } )
        .WillOnce( SetPhysicalDevices{ fake_physical_devices, VkResult::VK_SUCCESS } );
#else
    EXPECT_CALL( *mock_vulkan, enumerate_physical_devices )
        .InSequence( s )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) );
#endif

    EXPECT_EQ( result, app.initialize( ) );

    // Cleanup
    EXPECT_CALL( *mock_vulkan, destroy_surface ).InSequence( s ).WillOnce( testing::Return( ) );
    EXPECT_CALL( *mock_vulkan, destroy_instance ).InSequence( s ).WillOnce( testing::Return( ) );
}
#else

TEST_F( MoaWindowedAppTests, InitializeSuccess )
{
    auto app = apps::MoaWindowedApp{ mock_app, mock_vulkan };

    auto const arg = testing::Matcher< vlk::VulkanSetupDataWithSurface& >( testing::_ );
    EXPECT_CALL( mock_vulkan_setup, initialize( arg ) )
        .WillOnce( testing::Return( Result::success( ) ) );

    EXPECT_EQ( Result::success( ), app.initialize( mock_vulkan_setup ) );
}

TEST_F( MoaWindowedAppTests, InitializeSetupFailure )
{
    auto app = apps::MoaWindowedApp{ mock_app, mock_vulkan };

    auto constexpr result = Result{ ResultType::VlkInstanceCreationFailed };

    auto const arg = testing::Matcher< vlk::VulkanSetupDataWithSurface& >( testing::_ );
    EXPECT_CALL( mock_vulkan_setup, initialize( arg ) ).WillOnce( testing::Return( result ) );

    EXPECT_EQ( result, app.initialize( mock_vulkan_setup ) );
}

TEST_F( MoaWindowedAppTests, RunSuccess )
{
    auto app = apps::MoaWindowedApp{ mock_app, mock_vulkan };

    // Make sure the call happen in this exact order.
    auto const s = ::testing::Sequence{ };
    EXPECT_CALL( mock_app, should_exit ).InSequence( s ).WillOnce( testing::Return( false ) );
    EXPECT_CALL( mock_app, poll_inputs ).InSequence( s ).WillOnce( testing::Return( ) );
    EXPECT_CALL( mock_app, should_exit ).InSequence( s ).WillOnce( testing::Return( false ) );
    EXPECT_CALL( mock_app, poll_inputs ).InSequence( s ).WillOnce( testing::Return( ) );
    EXPECT_CALL( mock_app, should_exit ).InSequence( s ).WillOnce( testing::Return( true ) );

    app.run( );
}

TEST_F( MoaWindowedAppTests, RunOnce )
{
    auto app = apps::MoaWindowedApp{ mock_app, mock_vulkan };
    EXPECT_CALL( mock_app, should_exit ).WillOnce( testing::Return( true ) );
    app.run( );
}
#endif

} // namespace
} // namespace ltb
