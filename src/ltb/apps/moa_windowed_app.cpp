// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#include "ltb/apps/moa_windowed_app.hpp"

// project
#include "ltb/vlk/physical_device.hpp"

namespace ltb::apps
{

MoaWindowedApp::MoaWindowedApp(
    display::WindowedAppInterface& windowed_app,
    vlk::VulkanInterface&          vulkan
)
    : setup_{ windowed_app, vulkan }
{
}

auto MoaWindowedApp::initialize( vlk::VulkanSetupInterface& setup ) -> Result
{
    LTB_CHECK( setup.initialize( setup_ ) );
    return Result::success( );
}

auto MoaWindowedApp::run( ) -> void
{
    while ( !setup_.windowed_app.should_exit( ) )
    {
        setup_.windowed_app.poll_inputs( );
    }
}

} // namespace ltb::apps
