// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////

// project
#include "ltb/apps/moa_headless_app.hpp"
#include "ltb/test/mock_app.hpp"
#include "ltb/test/mock_vulkan.hpp"
#include "ltb/test/mock_vulkan_setup.hpp"

// external
#include <gmock/gmock.h>

namespace ltb
{
namespace
{

class MoaHeadlessAppTests : public ::testing::Test
{
public:
    testing::StrictMock< test::MockApp >         mock_app          = { };
    testing::StrictMock< test::MockVulkan >      mock_vulkan       = { };
    testing::StrictMock< test::MockVulkanSetup > mock_vulkan_setup = { };
};

TEST_F( MoaHeadlessAppTests, InitializeSuccess )
{
    auto app = apps::MoaHeadlessApp{ mock_app, mock_vulkan };

    EXPECT_CALL( mock_app, initialize ).WillOnce( testing::Return( Result::success( ) ) );

    auto const arg = testing::Matcher< vlk::VulkanSetupData& >( testing::_ );
    EXPECT_CALL( mock_vulkan_setup, initialize( arg ) )
        .WillOnce( testing::Return( Result::success( ) ) );

    EXPECT_EQ( Result::success( ), app.initialize( mock_vulkan_setup ) );
}

TEST_F( MoaHeadlessAppTests, InitializeAppFailure )
{
    auto app = apps::MoaHeadlessApp{ mock_app, mock_vulkan };

    auto constexpr result = Result{ ResultType::AppBackendInitFailed };

    EXPECT_CALL( mock_app, initialize ).WillOnce( testing::Return( result ) );

    EXPECT_EQ( result, app.initialize( mock_vulkan_setup ) );
}

TEST_F( MoaHeadlessAppTests, InitializeSetupFailure )
{
    auto app = apps::MoaHeadlessApp{ mock_app, mock_vulkan };

    auto constexpr result = Result{ ResultType::VlkInstanceCreationFailed };

    EXPECT_CALL( mock_app, initialize ).WillOnce( testing::Return( Result::success( ) ) );

    auto const arg = testing::Matcher< vlk::VulkanSetupData& >( testing::_ );
    EXPECT_CALL( mock_vulkan_setup, initialize( arg ) ).WillOnce( testing::Return( result ) );

    EXPECT_EQ( result, app.initialize( mock_vulkan_setup ) );
}

TEST_F( MoaHeadlessAppTests, RunSuccess )
{
    auto app = apps::MoaHeadlessApp{ mock_app, mock_vulkan };

    // Make sure the call happen in this exact order.
    auto const s = ::testing::Sequence{ };
    EXPECT_CALL( mock_app, should_exit ).InSequence( s ).WillOnce( testing::Return( false ) );
    EXPECT_CALL( mock_app, poll_inputs ).InSequence( s ).WillOnce( testing::Return( ) );
    EXPECT_CALL( mock_app, should_exit ).InSequence( s ).WillOnce( testing::Return( false ) );
    EXPECT_CALL( mock_app, poll_inputs ).InSequence( s ).WillOnce( testing::Return( ) );
    EXPECT_CALL( mock_app, should_exit ).InSequence( s ).WillOnce( testing::Return( true ) );

    app.run( );
}

TEST_F( MoaHeadlessAppTests, RunOnce )
{
    auto app = apps::MoaHeadlessApp{ mock_app, mock_vulkan };
    EXPECT_CALL( mock_app, should_exit ).WillOnce( testing::Return( true ) );
    app.run( );
}

} // namespace
} // namespace ltb
