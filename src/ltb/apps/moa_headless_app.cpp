// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#include "ltb/apps/moa_headless_app.hpp"

namespace ltb::apps
{

MoaHeadlessApp::MoaHeadlessApp( display::AppInterface& app, vlk::VulkanInterface& vulkan )
    : app_{ app }
    , setup_{ vulkan }
{
}

auto MoaHeadlessApp::initialize( vlk::VulkanSetupInterface& setup ) -> Result
{
    LTB_CHECK( app_.initialize( ) );
    LTB_CHECK( setup.initialize( setup_ ) );
    return Result::success( );
}

auto MoaHeadlessApp::run( ) -> void
{
    while ( !app_.should_exit( ) )
    {
        app_.poll_inputs( );
    }
}

} // namespace ltb::apps
