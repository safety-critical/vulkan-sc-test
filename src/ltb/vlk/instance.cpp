// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#include "ltb/vlk/instance.hpp"

// project
#include "ltb/utils/ignore.hpp"
#include "ltb/vlk/enumerate.hpp"

namespace ltb::vlk
{
namespace
{

struct InstanceDeleter
{
    VulkanInterface& vulkan;

    void operator( )( VkInstance_T* const instance ) const
    {
        vulkan.destroy_instance( instance, nullptr );
        spdlog::debug( "vkDestroyInstance()" );
    }
};

struct DebugUtilsMessengerDeleter
{
    VkInstance_T&                       instance;
    PFN_vkDestroyDebugUtilsMessengerEXT destroy_debug_messenger;

    auto operator( )( VkDebugUtilsMessengerEXT_T* const debug_messenger ) const
    {
        destroy_debug_messenger( &instance, debug_messenger, nullptr );
        spdlog::debug( "vkDestroyDebugUtilsMessengerEXT()" );
    }
};

[[nodiscard]] auto make_default_create_info( )
{
    auto debug_create_info            = VkDebugUtilsMessengerCreateInfoEXT{ };
    debug_create_info.sType           = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    debug_create_info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT
                                      | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
                                      | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    debug_create_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
                                  | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
                                  | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    debug_create_info.pfnUserCallback = default_debug_callback;
    debug_create_info.pUserData       = nullptr;
    return debug_create_info;
}

} // namespace

Instance::Instance( VulkanInterface& vulkan )
    : vulkan_{ vulkan }
{
}

auto Instance::initialize( ) -> Result
{
    return initialize( Initializer{ } );
}

auto Instance::initialize( Initializer const& initializer ) -> Result
{
    LTB_CHECK( initialize_instance( initializer ) );
    LTB_CHECK( initialize_debug_messenger( initializer ) );
    return Result::success( );
}

auto Instance::initialize_surface(
    display::WindowedAppInterface& windowed_app,
    VkSurfaceKHR_T**               surface
) const -> Result
{
    return windowed_app.initialize_surface( instance_.get( ), surface );
}

auto Instance::destroy_surface( VkSurfaceKHR_T* const surface ) const -> void
{
    vulkan_.destroy_surface( instance_.get( ), surface, nullptr );
    spdlog::debug( "vkDestroySurfaceKHR()" );
}

auto Instance::enumerate_physical_devices( std::vector< VkPhysicalDevice >& physical_devices
) const -> Result
{
    LTB_VK_CHECK(
        enumerate_with_result(
            physical_devices,
            &VulkanInterface::enumerate_physical_devices,
            vulkan_,
            instance_.get( )
        ),
        ResultType::VlkPhysicalDeviceSelectionFailed
    );

    return Result::success( );
}

auto Instance::initialize_instance( Initializer const& initializer ) -> Result
{
    auto application_info               = VkApplicationInfo{ };
    application_info.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    application_info.pNext              = nullptr;
    application_info.pApplicationName   = initializer.app_name.c_str( );
    application_info.applicationVersion = VK_MAKE_API_VERSION( 0, 1, 0, 0 );
    application_info.pEngineName        = "No Engine";
    application_info.engineVersion      = VK_MAKE_API_VERSION( 0, 1, 0, 0 );
    application_info.apiVersion         = VK_API_VERSION_1_3;

    auto debug_create_info            = make_default_create_info( );
    debug_create_info.pfnUserCallback = initializer.debug_callback;

    auto create_info  = VkInstanceCreateInfo{ };
    create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    if ( nullptr != initializer.debug_callback )
    {
        create_info.pNext = &debug_create_info;
    }
    else
    {
        create_info.pNext = nullptr;
    }
    create_info.flags            = initializer.flags;
    create_info.pApplicationInfo = &application_info;

    using LayerCountType = decltype( create_info.enabledLayerCount );
    create_info.enabledLayerCount
        = static_cast< LayerCountType >( initializer.layer_names.size( ) );
    create_info.ppEnabledLayerNames = initializer.layer_names.data( );

    using ExtTypeCount = decltype( create_info.enabledExtensionCount );
    create_info.enabledExtensionCount
        = static_cast< ExtTypeCount >( initializer.extension_names.size( ) );
    create_info.ppEnabledExtensionNames = initializer.extension_names.data( );

    auto* instance = VkInstance{ };

    LTB_VK_CHECK(
        vulkan_.create_instance( &create_info, nullptr, &instance ),
        ResultType::VlkInstanceCreationFailed
    );

    instance_ = std::shared_ptr< VkInstance_T >( instance, InstanceDeleter{ vulkan_ } );

    return Result::success( );
}

auto Instance::initialize_debug_messenger( Initializer const& initializer ) -> Result
{
    if ( nullptr == initializer.debug_callback )
    {
        return Result::success( );
    }

    auto* const vkCreateDebugUtilsMessengerEXT
        = reinterpret_cast< PFN_vkCreateDebugUtilsMessengerEXT >(
            vulkan_.get_instance_proc_addr( instance_.get( ), "vkCreateDebugUtilsMessengerEXT" )
        );

    auto* const vkDestroyDebugUtilsMessengerEXT
        = reinterpret_cast< PFN_vkDestroyDebugUtilsMessengerEXT >(
            vulkan_.get_instance_proc_addr( instance_.get( ), "vkDestroyDebugUtilsMessengerEXT" )
        );

    if ( ( nullptr == vkCreateDebugUtilsMessengerEXT )
         || ( nullptr == vkDestroyDebugUtilsMessengerEXT ) )
    {
        return { ResultType::VlkDebugMessengerNotSupported };
    }

    auto debug_create_info            = make_default_create_info( );
    debug_create_info.pfnUserCallback = initializer.debug_callback;

    auto* debug_messenger = VkDebugUtilsMessengerEXT{ };

    LTB_VK_CHECK(
        vkCreateDebugUtilsMessengerEXT(
            instance_.get( ),
            &debug_create_info,
            nullptr,
            &debug_messenger
        ),
        ResultType::VlkDebugMessengerNotSupported
    );
    spdlog::debug( "vkCreateDebugUtilsMessengerEXT()" );
    debug_messenger_ = std::shared_ptr< VkDebugUtilsMessengerEXT_T >(
        debug_messenger,
        DebugUtilsMessengerDeleter{ *instance_, vkDestroyDebugUtilsMessengerEXT }
    );

    return Result::success( );
}

VKAPI_ATTR VkBool32 default_debug_callback(
    VkDebugUtilsMessageSeverityFlagBitsEXT const      message_severity,
    VkDebugUtilsMessageTypeFlagsEXT                   message_type,
    VkDebugUtilsMessengerCallbackDataEXT const* const callback_data,
    void*                                             user_data
)
{
    utils::ignore( message_type );
    utils::ignore( user_data );

    switch ( message_severity )
    {
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
            spdlog::debug( "Validation layer: {}", callback_data->pMessage );
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
            spdlog::info( "Validation layer: {}", callback_data->pMessage );
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
            spdlog::warn( "Validation layer: {}", callback_data->pMessage );
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
            spdlog::error( "Validation layer: {}", callback_data->pMessage );
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT:
            break;
    }

    return false;
}

} // namespace ltb::vlk
