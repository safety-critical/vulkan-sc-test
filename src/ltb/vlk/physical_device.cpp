// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#include "ltb/vlk/physical_device.hpp"

// project
#include "ltb/vlk/instance.hpp"
#include "ltb/vlk/physical_device_info.hpp"
#include "ltb/vlk/vulkan_interface.hpp"

// external
#include <spdlog/spdlog.h>

namespace ltb::vlk
{

PhysicalDeviceSelector::PhysicalDeviceSelector( VulkanInterface& vulkan, Instance& instance )
    : vulkan_{ vulkan }
    , instance_{ instance }
{
}

auto PhysicalDeviceSelector::initialize(
    VkPhysicalDevice& selected_physical_device,
    QueueFamilyMap&   queue_map
) const -> Result
{
    return initialize( Initializer{ }, selected_physical_device, queue_map );
}

auto PhysicalDeviceSelector::initialize(
    Initializer const& initializer,
    VkPhysicalDevice&  selected_physical_device,
    QueueFamilyMap&    queue_map
) const -> Result
{
    auto physical_devices = std::vector< VkPhysicalDevice >{ };
    LTB_CHECK(
        initialize_physical_devices( physical_devices ),
        "Failed to enumerate physical devices"
    );
    // The above function errors if there are no physical devices
    // (though this function will still run fine with no physical devices).

    auto const physical_device_count = physical_devices.size( );
    for ( auto i = 0UL; i < physical_device_count; ++i )
    {
        if ( nullptr == selected_physical_device )
        {
            auto* const physical_device = physical_devices[ i ];
            auto        physical_device_info
                = PhysicalDeviceInfo{ vulkan_, physical_device, initializer.surface };

            LTB_CHECK( physical_device_info.initialize( ) );

            if ( physical_device_info.are_extensions_supported( initializer.extensions )
                 && physical_device_info.are_features_supported( initializer.features )
                 && physical_device_info.is_surface_supported( )
                 && physical_device_info
                        .are_queue_families_supported( initializer.queue_types, queue_map ) )
            {
                selected_physical_device = physical_device;
            }
        }
    }

    if ( nullptr == selected_physical_device )
    {
        spdlog::error( "No physical devices found with required features" );
        return { ResultType::VlkPhysicalDeviceSelectionFailed };
    }
    else
    {
        // Log the selected physical device so users can verify it.
        auto properties = VkPhysicalDeviceProperties{ };
        vulkan_.get_physical_device_properties( selected_physical_device, &properties );
        spdlog::info( "Selected physical device: {}", properties.deviceName );

        return Result::success( );
    }
}

auto PhysicalDeviceSelector::initialize_physical_devices(
    std::vector< VkPhysicalDevice >& physical_devices
) const -> Result
{
    LTB_CHECK( instance_.enumerate_physical_devices( physical_devices ) );

    if ( physical_devices.empty( ) )
    {
        spdlog::error( "No physical devices found" );
        return { ResultType::VlkPhysicalDeviceSelectionFailed };
    }

    return Result::success( );
}

auto SelectPhysicalDevice::operator( )( VkSurfaceKHR_T* const surface ) const -> Result
{
    auto const select_physical_device = vlk::PhysicalDeviceSelector{ vulkan_, instance_ };

    auto initializer    = vlk::PhysicalDeviceSelector::Initializer{ };
    initializer.surface = surface;
    return select_physical_device.initialize( initializer, physical_device_, queue_families_ );
}

} // namespace ltb::vlk
