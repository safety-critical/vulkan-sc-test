// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#include "ltb/vlk/vulkan.hpp"

namespace ltb::vlk
{

auto Vulkan::get_instance_proc_addr( VkInstance_T* const instance, char const* const p_name ) const
    -> PFN_vkVoidFunction
{
    return ::vkGetInstanceProcAddr( instance, p_name );
}

auto Vulkan::enumerate_physical_devices(
    VkInstance_T* const     instance,
    uint32_t* const         p_physical_device_count,
    VkPhysicalDevice* const p_physical_devices
) const -> VkResult
{
    return ::vkEnumeratePhysicalDevices( instance, p_physical_device_count, p_physical_devices );
}

auto Vulkan::enumerate_device_extension_properties(
    VkPhysicalDevice_T* const    physical_device,
    uint32_t* const              p_extension_count,
    VkExtensionProperties* const p_extensions
) const -> VkResult
{
    return ::vkEnumerateDeviceExtensionProperties(
        physical_device,
        nullptr,
        p_extension_count,
        p_extensions
    );
}

auto Vulkan::create_instance(
    VkInstanceCreateInfo const* const  p_create_info,
    VkAllocationCallbacks const* const p_allocator,
    VkInstance_T** const               p_instance
) const -> VkResult
{
    return ::vkCreateInstance( p_create_info, p_allocator, p_instance );
}

auto Vulkan::destroy_instance(
    VkInstance_T* const                instance,
    VkAllocationCallbacks const* const p_allocator
) const -> void
{
    return ::vkDestroyInstance( instance, p_allocator );
}

auto Vulkan::destroy_surface(
    VkInstance_T* const                instance,
    VkSurfaceKHR_T* const              surface,
    VkAllocationCallbacks const* const p_allocator
) const -> void
{
    return ::vkDestroySurfaceKHR( instance, surface, p_allocator );
}

auto Vulkan::get_physical_device_properties(
    VkPhysicalDevice_T* const         physical_device,
    VkPhysicalDeviceProperties* const p_properties
) const -> void
{
    return ::vkGetPhysicalDeviceProperties( physical_device, p_properties );
}

auto Vulkan::get_physical_device_surface_support(
    VkPhysicalDevice_T* const physical_device,
    uint32_t const            queue_family_index,
    VkSurfaceKHR_T* const     surface,
    VkBool32* const           p_supported
) const -> VkResult
{
    return ::vkGetPhysicalDeviceSurfaceSupportKHR(
        physical_device,
        queue_family_index,
        surface,
        p_supported
    );
}

auto Vulkan::get_physical_device_surface_formats(
    VkPhysicalDevice_T* const physical_device,
    VkSurfaceKHR_T* const     surface,
    uint32_t* const           p_format_count,
    VkSurfaceFormatKHR* const p_formats
) const -> VkResult
{
    return ::
        vkGetPhysicalDeviceSurfaceFormatsKHR( physical_device, surface, p_format_count, p_formats );
}

auto Vulkan::get_physical_device_surface_present_modes(
    VkPhysicalDevice_T* const physical_device,
    VkSurfaceKHR_T* const     surface,
    uint32_t* const           p_present_mode_count,
    VkPresentModeKHR* const   p_present_modes
) const -> VkResult
{
    return ::vkGetPhysicalDeviceSurfacePresentModesKHR(
        physical_device,
        surface,
        p_present_mode_count,
        p_present_modes
    );
}

auto Vulkan::get_physical_device_features2(
    VkPhysicalDevice_T* const        physical_device,
    VkPhysicalDeviceFeatures2* const p_features
) const -> void
{
    return ::vkGetPhysicalDeviceFeatures2( physical_device, p_features );
}

auto Vulkan::get_physical_device_queue_family_properties(
    VkPhysicalDevice_T* const      physical_device,
    uint32_t* const                p_queue_family_count,
    VkQueueFamilyProperties* const p_queue_families
) const -> void
{
    return ::vkGetPhysicalDeviceQueueFamilyProperties(
        physical_device,
        p_queue_family_count,
        p_queue_families
    );
}

} // namespace ltb::vlk
