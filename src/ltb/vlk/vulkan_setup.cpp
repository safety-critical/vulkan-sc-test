// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#include "ltb/vlk/vulkan_setup.hpp"

// project
#include "ltb/utils/ignore.hpp"
#include "ltb/vlk/physical_device.hpp"

namespace ltb::vlk
{

auto VulkanSetup::initialize( VulkanSetupData& setup ) const -> Result
{
    LTB_CHECK( setup.instance.initialize( ) );

    auto const select_physical_device = vlk::PhysicalDeviceSelector{ setup.vulkan, setup.instance };
    LTB_CHECK( select_physical_device.initialize( setup.physical_device, setup.queue_families ) );
    setup.queue_families.enumerate_unique_queue_families( setup.unique_queue_families );

    return Result::success( );
}

auto VulkanSetup::initialize( VulkanSetupDataWithSurface& setup ) const -> Result
{
    LTB_CHECK( setup.windowed_app.initialize( ) );

    auto instance_initializer = vlk::Instance::Initializer{ };

    auto               window_extension_count = uint32{ 0 };
    auto const** const window_extensions
        = setup.windowed_app.get_vulkan_instance_extensions( &window_extension_count );

    utils::ignore(
        // Returned iterator can be ignored
        instance_initializer.extension_names.insert(
            instance_initializer.extension_names.end( ),
            window_extensions,
            window_extensions + window_extension_count
        )
    );

    LTB_CHECK( setup.instance.initialize( instance_initializer ) );
    LTB_CHECK( setup.surface.initialize( ) );
    LTB_CHECK( setup.surface.use( SelectPhysicalDevice{
        setup.vulkan,
        setup.instance,
        setup.physical_device,
        setup.queue_families
    } ) );
    setup.queue_families.enumerate_unique_queue_families( setup.unique_queue_families );

    return Result::success( );
}

} // namespace ltb::vlk
