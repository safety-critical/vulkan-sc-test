// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////

// project
#include "ltb/test/log_capture_test_fixture.hpp"
#include "ltb/test/mock_debug_utils_messenger.hpp"
#include "ltb/test/mock_vulkan.hpp"
#include "ltb/test/mock_windowed_app.hpp"
#include "ltb/utils/result.hpp"
#include "ltb/vlk/instance.hpp"

// external
#include <gmock/gmock.h>

struct VkSurfaceKHR_T
{
    // Fake Surface Type
};

namespace ltb
{
namespace
{

struct DebugUtilsMessenger
{
    // Keep this thread local in case multiple tests are running at once.
    // If there is a better way to do this (ie not a global pointer) I'm all for it.
    inline static thread_local test::MockDebugUtilsMessenger* mock = nullptr;
};

auto create_debug_utils_messenger(
    VkInstance_T* const                             instance,
    VkDebugUtilsMessengerCreateInfoEXT const* const p_create_info,
    VkAllocationCallbacks const* const              p_allocator,
    VkDebugUtilsMessengerEXT_T* const               p_debug_messenger
) -> VkResult
{
    return DebugUtilsMessenger::mock
        ->create_debug_utils_messenger( instance, p_create_info, p_allocator, p_debug_messenger );
}

inline auto destroy_debug_utils_messenger(
    VkInstance_T* const                instance,
    VkDebugUtilsMessengerEXT_T* const  p_debug_messenger,
    VkAllocationCallbacks const* const p_allocator
) -> void
{
    return DebugUtilsMessenger::mock
        ->destroy_debug_utils_messenger( instance, p_debug_messenger, p_allocator );
}

[[nodiscard]]
auto* create_debug_proc_addr( )
{
    return reinterpret_cast< PFN_vkVoidFunction >( create_debug_utils_messenger );
}

[[nodiscard]]
auto* destroy_debug_proc_addr( )
{
    return reinterpret_cast< PFN_vkVoidFunction >( destroy_debug_utils_messenger );
}

struct SetSurface
{
    VkSurfaceKHR_T& surface_;
    Result const&   returned_result;

    auto operator( )( VkInstance const&, VkSurfaceKHR* const surface ) const -> Result
    {
        *surface = &surface_;
        return returned_result;
    }
};

class InstanceTests : public ::testing::Test
{
public:
    testing::StrictMock< test::MockVulkan >              mock_vulkan                = { };
    testing::StrictMock< test::MockDebugUtilsMessenger > mock_debug_utils_messenger = { };

protected:
    void SetUp( ) override { DebugUtilsMessenger::mock = &mock_debug_utils_messenger; }

    void TearDown( ) override { DebugUtilsMessenger::mock = nullptr; }
};

TEST_F( InstanceTests, InitializeSuccessDefault )
{
    auto instance = vlk::Instance{ mock_vulkan };

    // Make sure the calls happen in this exact order.
    auto const s = ::testing::Sequence{ };

    // Instance initialization
    EXPECT_CALL( mock_vulkan, create_instance )
        .InSequence( s )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) );

#if !defined( LTB_DRIVEOS_DEVICE ) && !defined( NDEBUG ) && !defined( WIN32 )
    EXPECT_CALL( mock_vulkan, get_instance_proc_addr )
        .InSequence( s )
        .WillOnce( testing::Return( create_debug_proc_addr( ) ) );
    EXPECT_CALL( mock_vulkan, get_instance_proc_addr )
        .InSequence( s )
        .WillOnce( testing::Return( destroy_debug_proc_addr( ) ) );
    EXPECT_CALL( *DebugUtilsMessenger::mock, create_debug_utils_messenger )
        .InSequence( s )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) );

    EXPECT_EQ( Result::success( ), instance.initialize( ) );

    // Cleanup
    EXPECT_CALL( *DebugUtilsMessenger::mock, destroy_debug_utils_messenger )
        .InSequence( s )
        .WillOnce( testing::Return( ) );
#else
    EXPECT_EQ( Result::success( ), instance.initialize( ) );
#endif

    EXPECT_CALL( mock_vulkan, destroy_instance ).InSequence( s ).WillOnce( testing::Return( ) );
}

TEST_F( InstanceTests, InitializeSuccessWithCallbacks )
{
    auto instance = vlk::Instance{ mock_vulkan };

    // Make sure the calls happen in this exact order.
    auto const s = ::testing::Sequence{ };

    // Instance initialization
    EXPECT_CALL( mock_vulkan, create_instance )
        .InSequence( s )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) );
    EXPECT_CALL( mock_vulkan, get_instance_proc_addr )
        .InSequence( s )
        .WillOnce( testing::Return( create_debug_proc_addr( ) ) );
    EXPECT_CALL( mock_vulkan, get_instance_proc_addr )
        .InSequence( s )
        .WillOnce( testing::Return( destroy_debug_proc_addr( ) ) );
    EXPECT_CALL( *DebugUtilsMessenger::mock, create_debug_utils_messenger )
        .InSequence( s )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) );

    auto initializer           = vlk::Instance::Initializer{ };
    initializer.debug_callback = vlk::default_debug_callback;
    EXPECT_EQ( Result::success( ), instance.initialize( initializer ) );

    // Cleanup
    EXPECT_CALL( *DebugUtilsMessenger::mock, destroy_debug_utils_messenger )
        .InSequence( s )
        .WillOnce( testing::Return( ) );
    EXPECT_CALL( mock_vulkan, destroy_instance ).InSequence( s ).WillOnce( testing::Return( ) );
}

TEST_F( InstanceTests, InitializeSuccessWithoutCallbacks )
{
    auto instance = vlk::Instance{ mock_vulkan };

    // Make sure the calls happen in this exact order.
    auto const s = ::testing::Sequence{ };

    // Instance initialization
    EXPECT_CALL( mock_vulkan, create_instance )
        .InSequence( s )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) );

    auto initializer           = vlk::Instance::Initializer{ };
    initializer.debug_callback = nullptr;
    EXPECT_EQ( Result::success( ), instance.initialize( initializer ) );

    // Cleanup
    EXPECT_CALL( mock_vulkan, destroy_instance ).InSequence( s ).WillOnce( testing::Return( ) );
}

TEST_F( InstanceTests, FailedInstanceCreation )
{
    auto instance = vlk::Instance{ mock_vulkan };

    auto constexpr vk_result = VkResult::VK_ERROR_INCOMPATIBLE_DRIVER;
    auto constexpr result    = Result{ ResultType::VlkInstanceCreationFailed, vk_result };

    // Make sure the calls happen in this exact order.
    auto const s = ::testing::Sequence{ };

    // Instance initialization
    EXPECT_CALL( mock_vulkan, create_instance )
        .InSequence( s )
        .WillOnce( testing::Return( vk_result ) );

    EXPECT_EQ( result, instance.initialize( ) );
}

TEST_F( InstanceTests, InvalidCreateDebugAddress )
{
    auto instance = vlk::Instance{ mock_vulkan };

    auto constexpr result = Result{
        ResultType::VlkDebugMessengerNotSupported,
        VkResult::VK_SUCCESS,
    };

    // Make sure the calls happen in this exact order.
    auto const s = ::testing::Sequence{ };

    // Instance initialization
    EXPECT_CALL( mock_vulkan, create_instance )
        .InSequence( s )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) );
    EXPECT_CALL( mock_vulkan, get_instance_proc_addr )
        .InSequence( s )
        .WillOnce( testing::Return( nullptr ) );
    EXPECT_CALL( mock_vulkan, get_instance_proc_addr )
        .InSequence( s )
        .WillOnce( testing::Return( destroy_debug_proc_addr( ) ) );

    auto initializer           = vlk::Instance::Initializer{ };
    initializer.debug_callback = vlk::default_debug_callback;
    EXPECT_EQ( result, instance.initialize( initializer ) );

    // Cleanup
    EXPECT_CALL( mock_vulkan, destroy_instance ).InSequence( s ).WillOnce( testing::Return( ) );
}

TEST_F( InstanceTests, InvalidDestroyDebugAddress )
{
    auto instance = vlk::Instance{ mock_vulkan };

    auto constexpr result = Result{
        ResultType::VlkDebugMessengerNotSupported,
        VkResult::VK_SUCCESS,
    };

    // Make sure the calls happen in this exact order.
    auto const s = ::testing::Sequence{ };

    // Instance initialization
    EXPECT_CALL( mock_vulkan, create_instance )
        .InSequence( s )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) );
    EXPECT_CALL( mock_vulkan, get_instance_proc_addr )
        .InSequence( s )
        .WillOnce( testing::Return( create_debug_proc_addr( ) ) );
    EXPECT_CALL( mock_vulkan, get_instance_proc_addr )
        .InSequence( s )
        .WillOnce( testing::Return( nullptr ) );

    auto initializer           = vlk::Instance::Initializer{ };
    initializer.debug_callback = vlk::default_debug_callback;
    EXPECT_EQ( result, instance.initialize( initializer ) );

    // Cleanup
    EXPECT_CALL( mock_vulkan, destroy_instance ).InSequence( s ).WillOnce( testing::Return( ) );
}

TEST_F( InstanceTests, InvalidDebugAddresses )
{
    auto instance = vlk::Instance{ mock_vulkan };

    auto constexpr result = Result{
        ResultType::VlkDebugMessengerNotSupported,
        VkResult::VK_SUCCESS,
    };

    // Make sure the calls happen in this exact order.
    auto const s = ::testing::Sequence{ };

    // Instance initialization
    EXPECT_CALL( mock_vulkan, create_instance )
        .InSequence( s )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) );
    EXPECT_CALL( mock_vulkan, get_instance_proc_addr )
        .InSequence( s )
        .WillOnce( testing::Return( nullptr ) );
    EXPECT_CALL( mock_vulkan, get_instance_proc_addr )
        .InSequence( s )
        .WillOnce( testing::Return( nullptr ) );

    auto initializer           = vlk::Instance::Initializer{ };
    initializer.debug_callback = vlk::default_debug_callback;
    EXPECT_EQ( result, instance.initialize( initializer ) );

    // Cleanup
    EXPECT_CALL( mock_vulkan, destroy_instance ).InSequence( s ).WillOnce( testing::Return( ) );
}

TEST_F( InstanceTests, FailedDebugUtilsMessengerCreation )
{
    auto instance = vlk::Instance{ mock_vulkan };

    auto constexpr vk_result = VkResult::VK_ERROR_NOT_PERMITTED_KHR;
    auto constexpr result    = Result{ ResultType::VlkDebugMessengerNotSupported, vk_result };

    // Make sure the calls happen in this exact order.
    auto const s = ::testing::Sequence{ };

    // Instance initialization
    EXPECT_CALL( mock_vulkan, create_instance )
        .InSequence( s )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) );
    EXPECT_CALL( mock_vulkan, get_instance_proc_addr )
        .InSequence( s )
        .WillOnce( testing::Return( create_debug_proc_addr( ) ) );
    EXPECT_CALL( mock_vulkan, get_instance_proc_addr )
        .InSequence( s )
        .WillOnce( testing::Return( destroy_debug_proc_addr( ) ) );
    EXPECT_CALL( *DebugUtilsMessenger::mock, create_debug_utils_messenger )
        .InSequence( s )
        .WillOnce( testing::Return( vk_result ) );

    auto initializer           = vlk::Instance::Initializer{ };
    initializer.debug_callback = vlk::default_debug_callback;
    EXPECT_EQ( result, instance.initialize( initializer ) );

    // Cleanup
    EXPECT_CALL( mock_vulkan, destroy_instance ).InSequence( s ).WillOnce( testing::Return( ) );
}

TEST_F( InstanceTests, SuccessfulSurfaceCreation )
{
    auto app = testing::StrictMock< test::MockWindowedApp >{ };

    auto const instance = vlk::Instance{ mock_vulkan };

    auto fake_surface = VkSurfaceKHR_T{ };
    EXPECT_CALL( app, initialize_surface )
        .WillOnce( SetSurface{ fake_surface, Result::success( ) } );

    VkSurfaceKHR_T* surface = nullptr;
    EXPECT_EQ( Result::success( ), instance.initialize_surface( app, &surface ) );

    EXPECT_EQ( &fake_surface, surface );
}

TEST_F( InstanceTests, FailedSurfaceCreation )
{
    auto app = testing::StrictMock< test::MockWindowedApp >{ };

    auto const instance = vlk::Instance{ mock_vulkan };

    auto constexpr result = Result{
        ResultType::VlkSurfaceCreationFailed,
        VkResult::VK_ERROR_SURFACE_LOST_KHR,
    };

    EXPECT_CALL( app, initialize_surface ).WillOnce( testing::Return( result ) );

    VkSurfaceKHR_T* surface = nullptr;
    EXPECT_EQ( result, instance.initialize_surface( app, &surface ) );
}

TEST_F( InstanceTests, DestroySurface )
{
    auto const instance = vlk::Instance{ mock_vulkan };

    auto fake_surface = VkSurfaceKHR_T{ };
    EXPECT_CALL( mock_vulkan, destroy_surface ).WillOnce( testing::Return( ) );

    instance.destroy_surface( &fake_surface );
}

TEST_F( InstanceTests, EnumeratePhysicalDevicesSuccess )
{
    auto const instance = vlk::Instance{ mock_vulkan };

    EXPECT_CALL( mock_vulkan, enumerate_physical_devices )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) );

    auto physical_devices = std::vector< VkPhysicalDevice >{ };
    EXPECT_EQ( Result::success( ), instance.enumerate_physical_devices( physical_devices ) );
}

TEST_F( InstanceTests, EnumeratePhysicalDevicesFailedCount )
{
    auto const instance = vlk::Instance{ mock_vulkan };

    auto constexpr vk_result = VkResult::VK_ERROR_DEVICE_LOST;
    auto constexpr result    = Result{ ResultType::VlkPhysicalDeviceSelectionFailed, vk_result };

    EXPECT_CALL( mock_vulkan, enumerate_physical_devices ).WillOnce( testing::Return( vk_result ) );

    auto physical_devices = std::vector< VkPhysicalDevice >{ };
    EXPECT_EQ( result, instance.enumerate_physical_devices( physical_devices ) );
}

TEST_F( InstanceTests, EnumeratePhysicalDevicesFailedCopy )
{
    auto const instance = vlk::Instance{ mock_vulkan };

    auto constexpr vk_result = VkResult::VK_ERROR_DEVICE_LOST;
    auto constexpr result    = Result{ ResultType::VlkPhysicalDeviceSelectionFailed, vk_result };

    EXPECT_CALL( mock_vulkan, enumerate_physical_devices )
        .WillOnce( testing::Return( VkResult::VK_SUCCESS ) )
        .WillOnce( testing::Return( vk_result ) );

    auto physical_devices = std::vector< VkPhysicalDevice >{ };
    EXPECT_EQ( result, instance.enumerate_physical_devices( physical_devices ) );
}

TEST_F( InstanceTests, DefaultDebugCallback )
{
    auto log_capture = test::LogCapture( test::get_test_name( ), spdlog::level::trace );

    auto callback_data     = VkDebugUtilsMessengerCallbackDataEXT{ };
    callback_data.pMessage = "Custom message";

    EXPECT_FALSE( vlk::default_debug_callback(
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT,
        VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT,
        &callback_data,
        nullptr
    ) );
    EXPECT_EQ( "[debug] Validation layer: Custom message\n", log_capture.get_and_clear_logs( ) );

    EXPECT_FALSE( vlk::default_debug_callback(
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT,
        VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT,
        &callback_data,
        nullptr
    ) );
    EXPECT_EQ( "[info] Validation layer: Custom message\n", log_capture.get_and_clear_logs( ) );

    EXPECT_FALSE( vlk::default_debug_callback(
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT,
        VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT,
        &callback_data,
        nullptr
    ) );
    EXPECT_EQ( "[warning] Validation layer: Custom message\n", log_capture.get_and_clear_logs( ) );

    EXPECT_FALSE( vlk::default_debug_callback(
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
        VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT,
        &callback_data,
        nullptr
    ) );
    EXPECT_EQ( "[error] Validation layer: Custom message\n", log_capture.get_and_clear_logs( ) );

    EXPECT_FALSE( vlk::default_debug_callback(
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT,
        VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT,
        &callback_data,
        nullptr
    ) );
    EXPECT_TRUE( log_capture.get_and_clear_logs( ).empty( ) );
}

} // namespace
} // namespace ltb
