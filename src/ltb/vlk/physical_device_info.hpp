// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#pragma once

// project
#include "ltb/vlk/physical_device.hpp"
#include "ltb/vlk/queue_types.hpp"

// standard
#include <array>
#include <limits>
#include <optional>
#include <vector>

namespace ltb::vlk
{

class PhysicalDeviceInfo
{
public:
    explicit PhysicalDeviceInfo( VulkanInterface& vulkan, VkPhysicalDevice const& physical_device );
    explicit PhysicalDeviceInfo(
        VulkanInterface&        vulkan,
        VkPhysicalDevice const& physical_device,
        VkSurfaceKHR const&     surface
    );

    auto initialize( ) -> Result;

    [[nodiscard]]
    auto are_extensions_supported( std::vector< char const* > const& extensions ) const -> bool;

    [[nodiscard]]
    auto are_features_supported( std::vector< PhysicalDeviceFeature > const& features
    ) const -> bool;

    [[nodiscard]]
    auto is_surface_supported( ) const -> bool;

    template < typename Callback >
    [[nodiscard]]
    auto use_queue_families( Callback const& callback ) const;

    [[nodiscard]]
    auto are_queue_families_supported(
        std::vector< VkQueueFlagBits > const& queue_family_types,
        QueueFamilyMap&                       output_map
    ) const -> bool;

private:
    VulkanInterface&        vulkan_;
    VkPhysicalDevice const& physical_device_;
    VkSurfaceKHR const&     surface_;

    std::vector< VkExtensionProperties >   supported_extensions_         = { };
    VkPhysicalDeviceFeatures2              supported_features_           = { };
    std::vector< VkSurfaceFormatKHR >      surface_formats_              = { };
    std::vector< VkPresentModeKHR >        surface_present_modes_        = { };
    std::vector< VkQueueFamilyProperties > queue_families_               = { };
    std::vector< QueueIndex >              surface_queue_family_indices_ = { };

    auto initialize_surface_properties( ) -> Result;
};

// /////////////////////////////////////////////////////////////

template < typename Callback >
auto PhysicalDeviceInfo::use_queue_families( Callback const& callback ) const
{
    return callback( physical_device_, queue_families_ );
}

} // namespace ltb::vlk
