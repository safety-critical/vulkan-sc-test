// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////

// project
#include "ltb/test/mock_vulkan.hpp"
#include "ltb/test/mock_windowed_app.hpp"
#include "ltb/vlk/instance.hpp"
#include "ltb/vlk/surface.hpp"

// external
#include <gmock/gmock.h>

namespace ltb
{
namespace
{

class SurfaceTests : public ::testing::Test
{
public:
    testing::StrictMock< test::MockVulkan >      mock_vulkan       = { };
    testing::StrictMock< test::MockWindowedApp > mock_windowed_app = { };

    vlk::Instance instance = vlk::Instance{ mock_vulkan };
};

TEST_F( SurfaceTests, InitializeSuccess )
{
    auto surface = vlk::Surface{ mock_windowed_app, instance };

    // The windowed app creates the surface because it needs to know the window handle.
    EXPECT_CALL( mock_windowed_app, initialize_surface )
        .WillOnce( testing::Return( Result::success( ) ) );

    EXPECT_EQ( Result::success( ), surface.initialize( ) );

    // Vulkan can destroy the surface when it goes out of scope.
    EXPECT_CALL( mock_vulkan, destroy_surface ).WillOnce( testing::Return( ) );
}

TEST_F( SurfaceTests, InitializeFailure )
{
    auto surface = vlk::Surface{ mock_windowed_app, instance };

    auto constexpr result = Result{
        ResultType::VlkSurfaceCreationFailed,
        VkResult::VK_ERROR_FORMAT_NOT_SUPPORTED,
    };

    EXPECT_CALL( mock_windowed_app, initialize_surface ).WillOnce( testing::Return( result ) );

    EXPECT_EQ( result, surface.initialize( ) );

    // No destruction should happen because the surface was never created.
}

} // namespace
} // namespace ltb
