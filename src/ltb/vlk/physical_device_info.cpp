// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#include "ltb/vlk/physical_device_info.hpp"

// project
#include "ltb/vlk/enumerate.hpp"
#include "ltb/vlk/vulkan_interface.hpp"

// external
#include <spdlog/spdlog.h>

// standard
#include <algorithm>
#include <string>

namespace ltb::vlk
{
namespace
{
struct ExtensionMatchesName
{
    std::string_view const& extension;

    auto operator( )( VkExtensionProperties const& supported_extension ) const
    {
        return extension == supported_extension.extensionName;
    }
};

struct ContainsExtension
{
    std::vector< VkExtensionProperties > const& supported_extensions;

    auto operator( )( std::string_view const& extension ) const
    {
        return std::any_of(
            supported_extensions.begin( ),
            supported_extensions.end( ),
            ExtensionMatchesName{ extension }
        );
    }
};

struct FeatureSupported
{
    VkPhysicalDeviceFeatures2 const& supported_features;

    auto operator( )( PhysicalDeviceFeature const feature ) const -> bool
    {
        return supported_features.features.*feature;
    }
};

} // namespace

PhysicalDeviceInfo::PhysicalDeviceInfo(
    VulkanInterface&        vulkan,
    VkPhysicalDevice const& physical_device
)
    : PhysicalDeviceInfo( vulkan, physical_device, nullptr )
{
}

PhysicalDeviceInfo::PhysicalDeviceInfo(
    VulkanInterface&        vulkan,
    VkPhysicalDevice const& physical_device,
    VkSurfaceKHR const&     surface
)
    : vulkan_{ vulkan }
    , physical_device_{ physical_device }
    , surface_{ surface }
{
}

auto PhysicalDeviceInfo::initialize( ) -> Result
{
    supported_features_.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    vulkan_.get_physical_device_features2( physical_device_, &supported_features_ );

    LTB_VK_CHECK(
        enumerate_with_result(
            supported_extensions_,
            &VulkanInterface::enumerate_device_extension_properties,
            vulkan_,
            physical_device_
        ),
        ResultType::VlkPhysicalDeviceSelectionFailed,
        "Failed to enumerate supported device extensions"
    );

    enumerate(
        queue_families_,
        &VulkanInterface::get_physical_device_queue_family_properties,
        vulkan_,
        physical_device_
    );

    LTB_CHECK( initialize_surface_properties( ) );

    return Result::success( );
}

auto PhysicalDeviceInfo::are_extensions_supported( std::vector< char const* > const& extensions
) const -> bool
{
    return std::all_of(
        extensions.begin( ),
        extensions.end( ),
        ContainsExtension{ supported_extensions_ }
    );
}

auto PhysicalDeviceInfo::are_features_supported(
    std::vector< PhysicalDeviceFeature > const& features
) const -> bool
{
    return std::all_of(
        features.begin( ),
        features.end( ),
        // Check for each feature type in supported_features_
        FeatureSupported{ supported_features_ }
    );
}

auto PhysicalDeviceInfo::is_surface_supported( ) const -> bool
{
    auto const surface_exists = ( nullptr == surface_ );
    auto const surface_supported
        = ( !surface_formats_.empty( ) ) && ( !surface_present_modes_.empty( ) );
    return surface_exists || surface_supported;
}

auto PhysicalDeviceInfo::are_queue_families_supported(
    std::vector< VkQueueFlagBits > const& queue_family_types,
    QueueFamilyMap&                       output_map
) const -> bool
{
    output_map.clear( );

    auto const expected_types      = to_queue_types( queue_family_types, surface_ );
    auto       all_types_supported = false;

    auto const queue_family_count = queue_families_.size( );
    for ( auto queue_family_index = QueueIndex{ 0 }; queue_family_index < queue_family_count;
          ++queue_family_index )
    {
        if ( !all_types_supported )
        {
            output_map.add_supported_types(
                queue_family_types,
                queue_families_,
                surface_queue_family_indices_,
                queue_family_index
            );

            all_types_supported = output_map.all_types_supported( expected_types );
        }
    }

    return all_types_supported;
}

auto PhysicalDeviceInfo::initialize_surface_properties( ) -> Result
{
    if ( nullptr != surface_ )
    {
        LTB_VK_CHECK(
            enumerate_with_result(
                surface_formats_,
                &VulkanInterface::get_physical_device_surface_formats,
                vulkan_,
                physical_device_,
                surface_
            ),
            ResultType::VlkPhysicalDeviceSelectionFailed,
            "Failed to enumerate surface formats"
        );

        LTB_VK_CHECK(
            enumerate_with_result(
                surface_present_modes_,
                &VulkanInterface::get_physical_device_surface_present_modes,
                vulkan_,
                physical_device_,
                surface_
            ),
            ResultType::VlkPhysicalDeviceSelectionFailed,
            "Failed to enumerate surface present modes"
        );

        auto const queue_family_count = queue_families_.size( );
        for ( auto queue_family_index = QueueIndex{ 0 }; queue_family_index < queue_family_count;
              ++queue_family_index )
        {
            auto surface_support = VkBool32{ false };
            LTB_VK_CHECK(
                vulkan_.get_physical_device_surface_support(
                    physical_device_,
                    queue_family_index,
                    surface_,
                    &surface_support
                ),
                ResultType::VlkPhysicalDeviceSelectionFailed
            );

            if ( VK_TRUE == surface_support )
            {
                surface_queue_family_indices_.push_back( queue_family_index );
            }
        }
    }

    return Result::success( );
}

} // namespace ltb::vlk
