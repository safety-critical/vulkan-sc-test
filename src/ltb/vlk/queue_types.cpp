// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#include "ltb/vlk/queue_types.hpp"

// project
#include "ltb/utils/ignore.hpp"

// standard
#include <algorithm>

namespace ltb::vlk
{
namespace
{

struct ContainsType
{
    QueueFamilyMap const& queue_families;

    auto operator( )( QueueType const type ) const
    {
        return queue_families.get( type ).has_value( );
    }
};

struct ToQueueType
{
    auto operator( )( VkQueueFlagBits const flag ) const { return to_queue_type( flag ); }
};

struct TypeIsUnknown
{
    auto operator( )( QueueType const type ) const { return type == QueueType::Unknown; }
};

} // namespace

auto to_string( QueueType const type ) -> char const*
{
    switch ( type )
    {
        case QueueType::Unknown:
            return "Unknown";
        case QueueType::Graphics:
            return "Graphics";
        case QueueType::Compute:
            return "Compute";
        case QueueType::Transfer:
            return "Transfer";
        case QueueType::SparseBinding:
            return "SparseBinding";
        case QueueType::Protected:
            return "Protected";
        case QueueType::Surface:
            return "Surface";
        default:
            break;
    }
    return "Unknown";
}

auto to_queue_type( VkQueueFlagBits const flag ) -> QueueType
{
    switch ( flag )
    {
        case VkQueueFlagBits::VK_QUEUE_GRAPHICS_BIT:
            return QueueType::Graphics;
        case VkQueueFlagBits::VK_QUEUE_COMPUTE_BIT:
            return QueueType::Compute;
        case VkQueueFlagBits::VK_QUEUE_TRANSFER_BIT:
            return QueueType::Transfer;
        case VkQueueFlagBits::VK_QUEUE_SPARSE_BINDING_BIT:
            return QueueType::SparseBinding;
        case VkQueueFlagBits::VK_QUEUE_PROTECTED_BIT:
            return QueueType::Protected;
        default:
            break;
    }
    return QueueType::Unknown;
}

auto to_queue_types( std::vector< VkQueueFlagBits > const& vk_types, VkSurfaceKHR const& surface )
    -> std::vector< QueueType >
{
    auto types = std::vector< QueueType >{ };
    utils::ignore( std::transform(
        vk_types.begin( ),
        vk_types.end( ),
        std::back_inserter( types ),
        ToQueueType{ }
    ) );
    utils::ignore(
        types
            .erase( std::remove_if( types.begin( ), types.end( ), TypeIsUnknown{ } ), types.end( ) )
    );

    if ( nullptr != surface )
    {
        types.push_back( QueueType::Surface );
    }

    return types;
}

auto QueueFamilyMap::clear( ) -> void
{
    std::fill( queue_families_.begin( ), queue_families_.end( ), invalid_queue_index_ );
}

auto QueueFamilyMap::add_supported_types(
    std::vector< VkQueueFlagBits > const&         types,
    std::vector< VkQueueFamilyProperties > const& queue_families,
    std::vector< QueueIndex > const&              surface_queue_family_indices,
    uint32 const                                  queue_family_index
) -> void
{
    using FlagType = decltype( queue_families[ queue_family_index ].queueFlags );

    auto const queue_flags = queue_families[ queue_family_index ].queueFlags;

    for ( auto const type : types )
    {
        if ( 0 != ( queue_flags & static_cast< FlagType >( type ) ) )
        {
            this->set( to_queue_type( type ), queue_family_index );
        }
    }

    if ( std::find(
             surface_queue_family_indices.begin( ),
             surface_queue_family_indices.end( ),
             queue_family_index
         )
         != surface_queue_family_indices.end( ) )
    {
        this->set( QueueType::Surface, queue_family_index );
    }
}

auto QueueFamilyMap::all_types_supported( std::vector< QueueType > const& expected_types
) const -> bool
{
    return std::all_of( expected_types.begin( ), expected_types.end( ), ContainsType{ *this } );
}

auto QueueFamilyMap::get( QueueType const type ) const -> std::optional< QueueIndex >
{
    if ( auto const index = queue_families_[ static_cast< IndexType >( type ) ];
         index != invalid_queue_index_ )
    {
        return index;
    }
    return std::nullopt;
}

auto QueueFamilyMap::enumerate_unique_queue_families( std::set< QueueIndex >& unique_queue_families
) const -> void
{
    unique_queue_families = { queue_families_.begin( ), queue_families_.end( ) };

    // Ignore the returned number of items erased.
    utils::ignore( unique_queue_families.erase( invalid_queue_index_ ) );
}

auto QueueFamilyMap::set( QueueType const type, QueueIndex const index ) -> void
{
    queue_families_[ static_cast< IndexType >( type ) ] = index;
}

} // namespace ltb::vlk
