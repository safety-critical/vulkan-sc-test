// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////
#include "ltb/vlk/surface.hpp"

// project
#include "ltb/ltb_config.hpp"
#include "ltb/utils/ignore.hpp"
#include "ltb/vlk/enumerate.hpp"
#include "ltb/vlk/physical_device.hpp"

// external
#include <spdlog/spdlog.h>

namespace ltb::vlk
{

namespace
{

struct SurfaceDeleter
{
    Instance& instance;

    auto operator( )( VkSurfaceKHR_T* const surface ) const { instance.destroy_surface( surface ); }
};

} // namespace

Surface::Surface( display::WindowedAppInterface& windowed_app, vlk::Instance& instance )
    : windowed_app_{ windowed_app }
    , instance_{ instance }
{
}

auto Surface::initialize( ) -> Result
{
    auto* surface = VkSurfaceKHR{ };
    LTB_CHECK( instance_.initialize_surface( windowed_app_, &surface ) );

    surface_ = std::shared_ptr< VkSurfaceKHR_T >( surface, SurfaceDeleter{ instance_ } );

    return Result::success( );
}

} // namespace ltb::vlk
