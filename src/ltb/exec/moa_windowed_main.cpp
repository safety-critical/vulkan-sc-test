// /////////////////////////////////////////////////////////////
// A Logan Thomas Barnes project
// /////////////////////////////////////////////////////////////

// project
#include "ltb/apps/moa_windowed_app.hpp"
#include "ltb/display/glfw/glfw_windowed_app.hpp"
#include "ltb/vlk/vulkan.hpp"

// external
#include <spdlog/spdlog.h>

auto main( ) -> ltb::int32
{
    spdlog::set_level( spdlog::level::debug );

    auto app    = ltb::display::GlfwWindowedApp{ };
    auto vulkan = ltb::vlk::Vulkan{ };
    auto moa    = ltb::apps::MoaWindowedApp{ app, vulkan };

    auto setup = ltb::vlk::VulkanSetup{ };

    if ( auto const result = moa.initialize( setup ); result.type != ltb::ResultType::Success )
    {
        spdlog::error(
            "Failed to initialize MoaApp: ({}, {})",
            ltb::to_string( result.type ),
            ltb::to_string( result.result )
        );
        return EXIT_FAILURE;
    }

    moa.run( );

    return EXIT_SUCCESS;
}
